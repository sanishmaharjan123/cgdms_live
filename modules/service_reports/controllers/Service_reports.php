<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Service_reports
 *
 * Extends the Project_Controller class
 * 
 */

class Service_reports extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		// control('Service_reports');

		$this->load->model('job_cards/job_card_model');
		$this->lang->load('service_reports/service_reports');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('service_reports');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'service_reports';
		$this->load->view($this->_container,$data);
	}

	public function job_summary() {

		// Display Page
		$data['header'] = lang('job_summary');
		$data['page'] = $this->config->item('template_admin') . "job_summary";
		$data['module'] = 'service_reports';
		$this->load->view($this->_container,$data);

	}

	function get_jobSummary($group){
		if(is_admin()){
			$where = '';
		}else{
			$dealer_id = $this->dealer_id;
			$where = ' AND dealer_id = '. $dealer_id;
			$dealer_where = ' WHERE dealer_id = '. $dealer_id;
		}

		$post = $this->input->post('selection');
		$post = explode(" - ", $post);
		if($group == 1) {

			$date_range = array($post[0],$post[1]);

			/*Service Types*/
			$rawQuery = "SELECT generate_crosstab_sql_plain ( $$ SELECT v.vehicle_name || '('|| v.variant_name||')', v.vehicle_name, v.variant_name, v.service_type_name, COUNT (v.service_type_name) FROM view_report_grouped_jobcard v WHERE v.job_card_issue_date >= ? AND v.job_card_issue_date <= ? $where GROUP BY 1, 2, 3, 4 $$, $$ SELECT DISTINCT NAME FROM mst_service_types $$, 'INT', '\"VEHICLE\" TEXT, \"vehicle_name\" TEXT, \"variant_name\" TEXT' ) AS sqlstring";


			$rawQuery = $this->db->query($rawQuery, $date_range)->row();

			$query1 = $rawQuery->sqlstring;
			$data['success'] = true;

			$data = $this->db->query($query1)->result();

		} else {

			$date_range = array($post[0],$post[1],$post[0],$post[1],$post[0],$post[1],$post[0],$post[1],);
			/*Recieved Delivered Pending Ready*/
			$query2 = '
			SELECT jobcard_summary.vehicle_id, jobcard_summary.variant_id, jobcard_summary.deleted_at, jobcard_summary.vehicle_name, jobcard_summary.variant_name,
			( SELECT count(*) AS count FROM view_report_grouped_jobcard jobcard_summary_1 
			WHERE
			((jobcard_summary_1.job_card_issue_date >= ?) AND (jobcard_summary_1.job_card_issue_date <= ?)) AND 
			jobcard_summary.vehicle_id = jobcard_summary_1.vehicle_id AND 
			jobcard_summary.variant_id = jobcard_summary_1.variant_id 
			'.$where.') AS recieved,
			( SELECT count(*) AS count FROM view_report_grouped_jobcard jobcard_summary_1 
			WHERE
			(((jobcard_summary_1.issue_date IS NOT NULL) AND (jobcard_summary_1.job_card_issue_date >= ?)) AND (jobcard_summary_1.job_card_issue_date <= ?)) AND 
			jobcard_summary.vehicle_id = jobcard_summary_1.vehicle_id AND 
			jobcard_summary.variant_id = jobcard_summary_1.variant_id'.$where.') AS delivered,
			( SELECT count(*) AS count FROM view_report_grouped_jobcard jobcard_summary_1 
			WHERE
			((((jobcard_summary_1.closed_status = 1) AND (jobcard_summary_1.issue_date IS NULL)) AND (jobcard_summary_1.job_card_issue_date = ?)) AND (jobcard_summary_1.job_card_issue_date <= ?)) AND 
			jobcard_summary.vehicle_id = jobcard_summary_1.vehicle_id AND 
			jobcard_summary.variant_id = jobcard_summary_1.variant_id'.$where.') AS ready,
			( SELECT count(*) AS count FROM view_report_grouped_jobcard jobcard_summary_1 
			WHERE
			(((jobcard_summary_1.closed_status = 0) AND (jobcard_summary_1.job_card_issue_date = ?)) AND (jobcard_summary_1.job_card_issue_date <= ?)) AND 
			jobcard_summary.vehicle_id = jobcard_summary_1.vehicle_id AND 
			jobcard_summary.variant_id = jobcard_summary_1.variant_id'.$where.') AS pending 
			FROM view_report_grouped_jobcard jobcard_summary 
			'.$dealer_where.' 
			GROUP BY jobcard_summary.vehicle_id, jobcard_summary.variant_id, jobcard_summary.deleted_at, jobcard_summary.vehicle_name, jobcard_summary.variant_name;
			';
			$data = $this->db->query($query2, $date_range)->result();
		}

		echo json_encode($data);
	}

	function foc_reports($json = NULL) {

		if($json == 'json') {

			$post = $this->input->post('selection');
			$post = explode(" - ", $post);

			$this->job_card_model->_table = "view_report_grouped_jobcard";
			// $this->job_card_model->_table = "view_report_service_FOC_details";

			if(is_admin()){
				$where = array();
			}else{
				$where['dealer_id'] = $this->dealer_id;
			}
			$this->db->where('service_type',4);
			if($this->input->post('selection')) {
				$where['job_card_issue_date >='] = $post[0];
				$where['job_card_issue_date <='] = $post[1];
			}

			$rows = $this->job_card_model->findAll($where);

			$formatter = new \NumberFormatter('en_US', \NumberFormatter::SPELLOUT);
			$formatter->setTextAttribute(\NumberFormatter::DEFAULT_RULESET, "%spellout-ordinal");

			foreach ($rows as $key => &$value) {
				$value->service_no = ucfirst($formatter->format($value->service_count));
			}
			unset($value);
			
			echo json_encode(array('rows'=>$rows));
			exit;
		}

		// Display Page
		$data['header'] = lang('foc_reports');
		$data['page'] = $this->config->item('template_admin') . "foc_reports";
		$data['module'] = 'service_reports';
		$this->load->view($this->_container,$data);
	}

	function pdi_reports($json = NULL) {

		if($json == 'json') {

			$post = $this->input->post('selection');
			$post = explode(" - ", $post);
			if(is_admin()){
				$where = array();
			}else{
				$dealer_id = $this->dealer_id;
				$where['dealer_id'] = $dealer_id;
			}

			$this->job_card_model->_table = "view_report_grouped_jobcard";

			if($this->input->post('selection')) {
				$where['job_card_issue_date >='] = $post[0];
				$where['job_card_issue_date <='] = $post[1];
			}

			$this->db->where('service_type',8);
			$rows = $this->job_card_model->findAll($where);
			
			echo json_encode(array('rows'=>$rows));
			exit;
		}

		// Display Page
		$data['header'] = lang('pdi_reports');
		$data['page'] = $this->config->item('template_admin') . "pdi_reports";
		$data['module'] = 'service_reports';
		$this->load->view($this->_container,$data);
	}

	function mechanic_earning($json = NULL) {
		if($json == 'json') {

			$post = $this->input->post('selection');
			$post = explode(" - ", $post);
			$date_range = array();

			if($this->input->post('selection')) {
				$date_range = array($post[0],$post[1],);
			}

			if(is_admin()){
				$where = '';
			}else{
				$where = "dealer_id = {$this->dealer_id}";
			}

			if( empty($date_range)){
				$this->job_card_model->_table = "view_report_service_mechanic_earning";

				// $this->db->where('dealer_id', $this->dealer_id);
				$rows = $this->job_card_model->findAll($where);

			} else {
				$where = "AND j.".$where;

				$query = "SELECT e.first_name, e.middle_name, e.last_name, (((e.first_name) :: TEXT || ' ' :: TEXT) ||(e.last_name) :: TEXT) AS mechanic_name, e.designation_id, e.designation_name, SUM (bill.total_jobs) AS jobs, SUM (bill.vat_job) AS vat_job, osw.ow_payment, osw.ow_final_amount, osw.ow_margin, SUM (bill.total_jobs) + SUM (bill.vat_job) + COALESCE (osw.ow_payment, 0) + COALESCE (osw.ow_margin, 0) AS net_amount, j.dealer_id FROM view_employees e JOIN view_report_grouped_jobcard j ON j.mechanics_id = e. ID JOIN ser_billing_record bill ON j.jobcard_group = bill.jobcard_group LEFT JOIN ( SELECT ow.mechanics_id, SUM (ow.total_amount) AS ow_payment, SUM (ow.billing_final_amount) AS ow_final_amount, (SUM(ow.billing_final_amount) -(SUM(ow.total_amount)) :: DOUBLE PRECISION) AS ow_margin FROM ser_outside_work ow WHERE (ow.billing_final_amount IS NOT NULL) GROUP BY ow.mechanics_id ) osw ON ((e. ID = osw.mechanics_id)) WHERE ((e.designation_id = 4) AND(e.employee_type = 2)) AND (bill.issue_date >= ? AND bill.issue_date <= ?)  {$where} GROUP BY e.first_name, e.middle_name, e.last_name, e.designation_id, e.designation_name, osw.ow_payment, osw.ow_final_amount, osw.ow_margin, j.dealer_id";
				// $query = "SELECT e.first_name, e.middle_name, e.last_name, (((e.first_name) :: TEXT || ' ' :: TEXT ) || (e.last_name) :: TEXT ) AS mechanic_name, e.designation_id, e.designation_name, SUM (bill.total_jobs) AS jobs, SUM (bill.vat_job) AS vat_job, osw.ow_payment, osw.ow_final_amount, osw.ow_margin, ((( SUM (bill.total_jobs) + SUM (bill.vat_job)) + (osw.ow_payment) :: DOUBLE PRECISION ) + osw.ow_margin ) AS net_amount FROM ((( view_employees e JOIN view_report_grouped_jobcard j ON ((j.mechanics_id = e. ID))) JOIN ser_billing_record bill ON (( j.jobcard_group = bill.jobcard_group ))) LEFT JOIN ( SELECT ow.mechanics_id, SUM (ow.total_amount) AS ow_payment, SUM (ow.billing_final_amount) AS ow_final_amount, ( SUM (ow.billing_final_amount) - (SUM(ow.total_amount)) :: DOUBLE PRECISION ) AS ow_margin FROM ser_outside_work ow WHERE ( ow.billing_final_amount IS NOT NULL ) GROUP BY ow.mechanics_id ) osw ON ((e. ID = osw.mechanics_id))) WHERE ((e.designation_id = 4) AND (e.employee_type = 2)) AND ( bill.issue_date >= ? AND bill.issue_date <= ? ) AND e.$where GROUP BY e.first_name, e.middle_name, e.last_name, e.designation_id, e.designation_name, osw.ow_payment, osw.ow_final_amount, osw.ow_margin;";


				$rows = $this->db->query($query, $date_range)->result();
			}


			echo json_encode(array('rows'=>$rows));
			exit;
		}

		// Display Page
		$data['header'] = lang('mechanic_earning');
		$data['page'] = $this->config->item('template_admin') . "mechanic_earning";
		$data['module'] = 'service_reports';
		$this->load->view($this->_container,$data);
	}

	function counter_sales($json = NULL) {
		if($json == 'json') {

			$post = $this->input->post('selection');
			$post = explode(" - ", $post);
			$date_range = array();

			if($this->input->post('selection')) {
				$date_range = array($post[0],$post[1],);
			}

			if(is_admin()){
				$where = '';
			}else{
				$where = "dealer_id = {$this->dealer_id}";
			}

			if( empty($date_range)){
				$this->job_card_model->_table = "view_report_service_counter_sales";

				// $this->db->where('dealer_id', $this->dealer_id);
				$rows = $this->job_card_model->findAll($where);

			} else {
				$where = "AND csales.".$where;

				$query = " SELECT pcat. ID, pcat. NAME AS category_name, COUNT (cparts.quantity) AS category_count, SUM (cparts.quantity) AS category_quantity, ((SUM(cparts.price)) :: DOUBLE PRECISION - SUM(cparts.final_amount)) AS discount_amount, ow.warranty_price, SUM (cbills.vat_parts) AS vat_amount, SUM (cparts.price) AS parts_price, ((SUM(cbills.net_total) - SUM(cbills.vat_parts)) - (ow.warranty_price) :: DOUBLE PRECISION ) AS total, SUM (cbills.cash_discount_amt) AS cash_discount, csales.dealer_id, csales.deleted_at FROM (((((ser_counter_sales csales JOIN ser_parts cparts ON((cparts.bill_id = csales. ID))) JOIN mst_spareparts sp ON ((sp. ID = cparts.part_id))) JOIN mst_spareparts_category pcat ON ((sp.category_id = pcat. ID))) JOIN ser_billing_record cbills ON ((cbills. ID = csales.billing_record_id))) JOIN ( SELECT sp_1.category_id, SUM (pa.price) AS warranty_price FROM ((ser_parts pa JOIN mst_spareparts sp_1 ON((pa.part_id = sp_1. ID))) JOIN mst_spareparts_category scat ON ((sp_1.category_id = scat. ID))) WHERE ((pa.warranty IS NOT NULL) AND(pa.bill_id IS NOT NULL)) GROUP BY sp_1.category_id ) ow ON ((ow.category_id = pcat. ID))) WHERE csales.date_time BETWEEN ? AND ? {$where} GROUP BY pcat. ID, pcat. NAME, ow.warranty_price, csales.dealer_id, csales.deleted_at;";

				$rows = $this->db->query($query, $date_range)->result();
			}
			
			echo json_encode(array('rows'=>$rows));
			exit;
			
		}

		// Display Page
		$data['header'] = lang('counter_sales');
		$data['page'] = $this->config->item('template_admin') . "counter_sales";
		$data['module'] = 'service_reports';
		$this->load->view($this->_container,$data);	
	}

	function sales_summary($json = NULL) {
		if($json == 'json') {

			$post = $this->input->post('selection');
			$post = explode(" - ", $post);
			$date_range = array();

			if($this->input->post('selection')) {
				$date_range = array($post[0],$post[1],);
			}

			if(is_admin()){
				$where = '';
			}else{
				$where = "dealer_id = {$this->dealer_id}";
			}

			if( empty($date_range)){
				$this->job_card_model->_table = "view_report_service_sales_summary";
				// $this->db->where('dealer_id', $this->dealer_id);
				$rows = $this->job_card_model->findAll($where);

			} else {
				$where = "AND jcards.".$where;

				$query = "SELECT sparepart.category_id, s_category. NAME AS category_name, SUM (jparts.quantity) AS quantity, (SUM((((jparts.price * jparts.quantity) * jparts.discount_percentage) / 100))) :: DOUBLE PRECISION AS discount_amount, SUM ((jparts.price * jparts.quantity)) AS taxable, SUM (jparts.cash_discount) AS cash_discount, SUM ((jparts.final_amount *(0.13) :: DOUBLE PRECISION)) AS vat_amount, SUM (COALESCE(unwar.uw_amount,(0) :: REAL)) AS uw_amount, (((SUM((jparts.price * jparts.quantity))) :: DOUBLE PRECISION + COALESCE (SUM((jparts.final_amount *(0.13) :: DOUBLE PRECISION)),(0) :: DOUBLE PRECISION)) - COALESCE (SUM(unwar.uw_amount),(0) :: REAL)) AS net_amount, jcards.deleted_at, jcards.dealer_id, jcards.job_card_issue_date FROM (((( view_report_grouped_jobcard jcards JOIN ser_parts jparts ON ((jparts.jobcard_group = jcards.jobcard_serial))) JOIN mst_spareparts sparepart ON ((jparts.part_id = sparepart. ID))) JOIN mst_spareparts_category s_category ON ((sparepart.category_id = s_category. ID))) LEFT JOIN ( SELECT sp.category_id, pa.warranty, SUM (pa.final_amount) AS uw_amount FROM ((ser_parts pa JOIN mst_spareparts sp ON((pa.part_id = sp. ID))) JOIN mst_spareparts_category cat ON ((sp.category_id = cat. ID))) WHERE (pa.warranty IS NOT NULL) GROUP BY pa.warranty, sp.category_id ) unwar ON ((unwar.category_id = sparepart.category_id))) WHERE (((jparts.bill_id IS NULL) AND(jparts.estimate_id IS NULL)) AND(jparts.warranty IS NULL)) AND (jcards.job_card_issue_date BETWEEN ? AND ?) {$where} GROUP BY sparepart.category_id, s_category. NAME, jcards.deleted_at, jcards.dealer_id, jcards.job_card_issue_date;";

				$rows = $this->db->query($query, $date_range)->result();
			}
			
			echo json_encode(array('rows'=>$rows));
			exit;
		}
		
		// Display Page
		$data['header'] = lang('sales_summary');
		$data['page'] = $this->config->item('template_admin') . "sales_summary";
		$data['module'] = 'service_reports';
		$this->load->view($this->_container,$data);	

	}


	function dent_paint($json = NULL) {
		if($json == 'json') {

			$post = $this->input->post('selection');
			$post = explode(" - ", $post);
			$date_range = array();

			if($this->input->post('selection')) {
				$date_range = array($post[0],$post[1],);
			}
			if(is_admin()){
				$where = array();
			}else{
				$dealer_id = $this->dealer_id;
				$where['dealer_id'] = $dealer_id;
			}

			// if( empty($date_range)){
			$this->job_card_model->_table = "view_report_service_dent_paint";
			$rows = $this->job_card_model->findAll();

			/*} else {
				$query = " SELECT sp.category_id, c.name AS category_name, sum(cparts.quantity) AS quantity, (sum((((cparts.price * cparts.quantity) * cparts.discount_percentage) / 100)))::double precision AS discount_amount, sum((cparts.price * cparts.quantity)) AS taxable, sum(cparts.cash_discount) AS cash_discount, sum((cparts.final_amount * (0.13)::double precision)) AS taxes, sum(COALESCE(unwar.uw_amount, (0)::real)) AS uw_amount, (((sum((cparts.price * cparts.quantity)))::double precision + sum((cparts.final_amount * (0.13)::double precision))) - sum(COALESCE(unwar.uw_amount, (0)::real))) AS net_amount, csales.deleted_at FROM (((((ser_counter_sales csales JOIN ser_parts cparts ON ((cparts.bill_id = csales.id))) JOIN ser_billing_record cbills ON ((csales.billing_record_id = cbills.id))) JOIN mst_spareparts sp ON ((cparts.part_id = sp.id))) JOIN mst_spareparts_category c ON ((sp.category_id = c.id))) LEFT JOIN ( SELECT sp_1.category_id, pa.warranty, sum(pa.final_amount) AS uw_amount FROM ((ser_parts pa JOIN mst_spareparts sp_1 ON ((pa.part_id = sp_1.id))) JOIN mst_spareparts_category cat ON ((sp_1.category_id = cat.id))) WHERE (pa.warranty IS NOT NULL) GROUP BY pa.warranty, sp_1.category_id) unwar ON ((unwar.category_id = sp.category_id))) WHERE ((((cparts.bill_id IS NOT NULL) AND (cparts.estimate_id IS NULL)) AND (cparts.warranty IS NULL)) AND (cparts.jobcard_group IS NULL) and (csales.date_time between ? and ?)) and ( 'deleted_at' IS NULL) GROUP BY sp.category_id, c.name, csales.deleted_at;";

				$rows = $this->db->query($query, $date_range)->result();
			}*/
			
			echo json_encode(array('rows'=>$rows));
			exit;
			
		}

		// Display Page
		$data['header'] = lang('dent_paint');
		$data['page'] = $this->config->item('template_admin') . "dent_paint";
		$data['module'] = 'service_reports';
		$this->load->view($this->_container,$data);	
	}

	function mechanic_consume($json = NULL) {
		if($json == 'json') {

			$post = $this->input->post('selection');
			$post = explode(" - ", $post);
			$date_range = array();

			if($this->input->post('selection')) {
				$date_range = array($post[0],$post[1],);
			}

			// if( empty($date_range)){
			$rawQuery = "SELECT generate_crosstab_sql_plain ( $$ SELECT e. ID, e.mechanic_name, e.jobs, e.vat_job, e.ow_payment, e.ow_margin, e.ow_final_amount * 0.13 AS ow_tax, e.category_name, e.cat_amt FROM view_report_service_mechanic_consume_helper e GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9 $$, $$ SELECT mst_spareparts_category. NAME FROM mst_spareparts_category $$, 'FLOAT', '\"id\" TEXT, \"mechanic_name\" TEXT, \"taxable\" FLOAT,\"taxes\" FLOAT, \"ow_payment\" FLOAT, \"ow_margin\" FLOAT, \"ow_tax\" FLOAT' ) AS sqlstring";

			$rawQuery = $this->db->query($rawQuery, $date_range)->row();

			$query = $rawQuery->sqlstring;

			$rows = $this->db->query($query)->result();
			/*} else {
			$this->job_card_model->_table = "view_report_service_dent_paint";
			$rows = $this->job_card_model->findAll();

		]}*/

		echo json_encode(array('rows'=>$rows));
		exit;

	}

		// Display Page
	$data['header'] = lang('mechanic_consume');
	$data['page'] = $this->config->item('template_admin') . "mechanic_consume";
	$data['module'] = 'service_reports';
	$this->load->view($this->_container,$data);	
}



}