    <style>
        table.form-table td:nth-child(odd){
            width: 10% !important;
        }
        table.form-table td:nth-child(even){
            width: 2% !important;
        }
        .textbox {
            height: 100px;
            width: 200px;
            border-radius: 5px;
            border-color: #ccc;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo lang('dealer_orders'); ?></h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active"><?php echo lang('dealer_orders'); ?></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- row -->
            <div class="row">
                <div class="col-xs-12 connectedSortable">
                    <?php echo displayStatus(); ?>
                <!-- <div id='jqxGridDealer_orderToolbar' class='grid-toolbar'>
                    <button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDealer_orderInsert"><?php echo lang('general_create'); ?></button>
                    <button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDealer_orderFilterClear"><?php echo lang('general_clear'); ?></button>
                </div> -->
                <div id="jqxGridDealer_order"></div>
            </div><!-- /.col -->
        </div>
        <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<div id="jqxPopupWindowCredit_approve">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title'>Approval Form</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-Credit_approve', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "approval_order_id"/>
        <table class="form-table app-table">           
            <tr>
                <td><label for="payment_detail">Payment Detail</label></td>
                <td>:</td>
                <td><span id="payment_details"></span></td>       
            </tr>
            <!-- <tr>
                <td><label for = "approve_limit">Approve Quantity</label></td>
                <td>:</td>
                <td><input type="text" name="approve_quantity" class="text_input" ></td>
            </tr> -->
            <tr id="remarks_credit" style="display: none;">
                <td><label>Remarks</label></td>
                <td>:</td>
                <td><textarea name="remarks_credit" class="textbox" id = 'remarks' hidden="true"></textarea></td>
            </tr>
            <tr> </tr>
            <tr>
                <th colspan="3">
                    <button type="button" class="btn btn-success btn-md btn-flat" id="jqxCredit_approveSubmitButton"><?php echo "Confirm"//lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-md btn-flat" id="jqxCredit_approveCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="jqxPopupWindowCredit_cancel">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title'>Cancel Form</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-Credit_cancel', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "cancel_id" id = "cancel_order_id"/>
        <table class="form-table app-table">
            <tr>
                <td>
                    <label><span>Are you sure you want to cancel?</span></label>
                </td>
            </tr>
            <tr>
                <th colspan="3">
                    <button type="button" class="btn btn-success btn-md btn-flat" id="jqxCredit_cancelSubmitButton"><?php echo "YES"//lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-md btn-flat" id="jqxCredit_cancelCancelButton"><?php echo "NO"//lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="jqxPopupWindowCredit_edit">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title'>edit Form</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-Credit_edit', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "edit_order_id"/>
        <table class="form-table app-table">           
            <tr>
                <td><label for="payment_detail">Payment Detail</label></td>
                <td>:</td>
                <td>
                    <select name="payment_method" id="payment_dropdown">
                        <option default>Select a Value</option>
                        <option value="cash">Cash/others</option>
                        <option value="bg">BG</option>
                        <option value="lc">LC</option>
                        <option value="cheque">Cheque</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label>Payment value</label></td>
                <td>:</td>
                <td><input name="payment_value" class="text_input" id = 'payment_value'></td>
            </tr>
            <tr> </tr>
            <tr>
                <th colspan="3">
                    <button type="button" class="btn btn-success btn-md btn-flat" id="jqxCredit_editSubmitButton"><?php echo lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-md btn-flat" id="jqxCredit_editCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="jqxPopupWindowCredit_reject">
    <div class='jqxExpander-custom-div'>
        <span class='popup_title'>Reject Form</span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' => 'form-Credit_reject', 'onsubmit' => 'return false')); ?>
        <input type = "hidden" name = "id" id = "reject_order_id"/>
        <input type = "hidden" name = "dealer_id" id = "reject_dealer_id"/>
        <table class="form-table app-table">           
            <tr>
                <td><label for="payment_detail">Payment Detail</label></td>
                <td>:</td>
                <td><span id="payment_details_reject"></span></td>       
            </tr>
            <tr id="remarks_credit">
                <td><label>Remarks</label></td>
                <td>:</td>
                <td><textarea name="remarks_credit" class="textbox" id = 'reject_remarks'></textarea></td>
            </tr>
            <tr> </tr>
            <tr>
                <th colspan="3">
                    <button type="button" class="btn btn-success btn-md btn-flat" id="jqxCredit_rejectSubmitButton"><?php echo "Confirm"//lang('general_save'); ?></button>
                    <button type="button" class="btn btn-default btn-md btn-flat" id="jqxCredit_rejectCancelButton"><?php echo lang('general_cancel'); ?></button>
                </th>
            </tr>

        </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

    $(function () {

        var dealer_ordersDataSource =
        {
            datatype: "json",
            datafields: [
            {name: 'vehicle_id', type: 'number'},
            {name: 'color_id', type: 'number'},
            {name: 'date_of_order', type: 'date'},
            {name: 'date_of_delivery', type: 'date'},
            {name: 'delivery_lead_time', type: 'string'},
            {name: 'pdi_status', type: 'number'},
            {name: 'date_of_retail', type: 'date'},
            {name: 'retail_lead_time', type: 'string'},
            {name: 'variant_id', type: 'number'},
            {name: 'vehicle_name', type: 'string'},
            {name: 'variant_name', type: 'string'},
            {name: 'color_name', type: 'string'},
            {name: 'payment_status', type: 'string'},
            {name: 'received_date', type: 'date'},
            {name: 'payment_method', type: 'string'},
            {name: 'associated_value_payment', type: 'string'},
            {name: 'quantity', type: 'number'},
            {name: 'order_id', type: 'number'},
            {name: 'stock_dispatch_date', type: 'date'},
            {name: 'year', type: 'number'},
            {name: 'total_dispatched', type: 'number'},
            {name: 'credit_approval', type: 'string'},
            {name: 'payment_detail', type: 'string'},
            {name: 'dealer_name', type: 'string'},
            {name: 'credit_control_age', type: 'number'},
            {name: 'dealer_id', type: 'number'},
            ],
            url: '<?php echo site_url("admin/dealer_orders/credit_control_json"); ?>',
            pagesize: defaultPageSize,
            root: 'rows',
            id: 'id',
            cache: true,
            pager: function (pagenum, pagesize, oldpagenum) {
            },
            beforeprocessing: function (data) {
                dealer_ordersDataSource.totalrecords = data.total;
            },
            filter: function () {
                $("#jqxGridDealer_order").jqxGrid('updatebounddata', 'filter');
            },
            sort: function () {
                $("#jqxGridDealer_order").jqxGrid('updatebounddata', 'sort');
            },
            processdata: function (data) {
            }
        };

        $("#jqxGridDealer_order").jqxGrid({
            theme: theme,
            width: '100%',
            height: gridHeight,
            source: dealer_ordersDataSource,
            altrows: true,
            pageable: true,
            sortable: true,
            rowsheight: 30,
            columnsheight: 30,
            showfilterrow: true,
            filterable: true,
            columnsresize: true,
            autoshowfiltericon: true,
            columnsreorder: true,
            selectionmode: 'multiplecellsadvanced',
            virtualmode: true,
            enableanimations: false,
            pagesizeoptions: pagesizeoptions,
            showtoolbar: true,
            rendertoolbar: function (toolbar) {
                var container = $("<div style='margin: 5px; height:50px'></div>");
                container.append($('#jqxGridDealer_orderToolbar').html());
                toolbar.append(container);
            },
            columns: [
            {text: 'SN', width: 50, pinned: true, exportable: false, columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer, filterable: false},
            {
                text: 'Action', datafield: 'action', width: 60, sortable: false, filterable: false, pinned: true, align: 'center', cellsalign: 'center', cellclassname: 'grid-column-center',
                cellsrenderer: function (index) {
                    var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
                    var e = '';
                    if(row.credit_control_approval != 1){
                        e = '<a href="javascript:void(0)" onclick="credit_approve(' + index  + '); return false;" title="Approve"><i class="fa fa-check"></i></a> &nbsp';
                    }
                    if(row.credit_control_approval ==1)
                    {
                        <?php if(is_admin()): ?>
                        e += '<a href="javascript:void(0)" onclick="credit_cancel(' + row.order_id + '); return false;" title="Cancel Approval"><i class="fa fa-times"></i></a> &nbsp';
                    <?php endif;?>
                }
                
                if(row.credit_control_approval != 2)
                {
                    e += '<a href="javascript:void(0)" onclick="credit_reject(' + index + '); return false;" title="Reject"><i class="fa fa-ban"></i></a> &nbsp';
                }
                e += '<a href="javascript:void(0)" onclick="credit_edit(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
                return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
            }
        },
        {text: '<?php echo lang("dealer_name"); ?>', datafield: 'dealer_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
        {text: '<?php echo lang("model_id"); ?>', datafield: 'vehicle_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
        {text: '<?php echo lang("variant_id"); ?>', datafield: 'variant_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
        {text: '<?php echo lang("color_id"); ?>', datafield: 'color_name', width: 150, filterable: true, renderer: gridColumnsRenderer},
        {text: '<?php echo "Order Quantity"?>', datafield: 'quantity', width: 150, filterable: true, renderer: gridColumnsRenderer},
        {text: '<?php echo "Payment Details"?>', datafield: 'payment_detail', width: 150, filterable: true, renderer: gridColumnsRenderer},
        {text: '<?php echo "Approval Status" ?>', datafield: 'credit_approval', width: 150, filterable: true, renderer: gridColumnsRenderer}, 
        {text: '<?php echo "Order Ageing" ?>', datafield: 'credit_control_age', width: 150, filterable: true, renderer: gridColumnsRenderer},
        ],
        rendergridrows: function (result) {
            return result.data;
        }
    });

        $("[data-toggle='offcanvas']").click(function (e) {
            e.preventDefault();
            setTimeout(function () {
                $("#jqxGridDealer_order").jqxGrid('refresh');
            }, 500);
        });

        $(document).on('click', '#jqxGridDealer_orderFilterClear', function () {
            $('#jqxGridDealer_order').jqxGrid('clearfilters');
        });

        $(document).on('click', '#jqxGridDealer_orderInsert', function () {
            openPopupWindow('jqxPopupWindowDealer_order', '<?php echo lang("general_add") . "&nbsp;" . $header; ?>');
        });
    });

        // Cancel Order
        $("#jqxPopupWindowCredit_approve").jqxWindow({
            theme: theme,
            width: '40%',
            maxWidth: '40%',
            height: '50%',
            maxHeight: '50%',
            isModal: true,
            autoOpen: false,
            modalOpacity: 0.7,
            showCollapseButton: false
        });

        $("#jqxPopupWindowCredit_approve").on('close', function () {
        });

        $('#form-Credit_approve').jqxValidator({
            hintType: 'label',
            animationDuration: 500,
            rules: [

            { input: '#remarks', message: 'Required', action: 'blur', 
            rule: function(input) {
                val = $('#remarks').val();
                var hidden = $('#remarks').attr('hidden');
                if(hidden == 'hidden')
                {
                    return true;
                }
                else
                {
                    return (val == '' || val == null || val == 0) ? false: true;
                }
            }
        }]
    });

        $("#jqxCredit_approveSubmitButton").on('click', function () {
           var validationResult = function (isValid) {
            if (isValid) {
               save_Credit_approve();
           }
       };
       $('#form-Credit_approve').jqxValidator('validate', validationResult);

   });
        $("#jqxCredit_approveCancelButton").on('click', function () {
            $('#jqxPopupWindowCredit_approve').jqxWindow('close');
        });

        // credit edit
        $("#jqxPopupWindowCredit_edit").jqxWindow({
            theme: theme,
            width: '40%',
            maxWidth: '40%',
            height: '50%',
            maxHeight: '50%',
            isModal: true,
            autoOpen: false,
            modalOpacity: 0.7,
            showCollapseButton: false
        });

        $("#jqxPopupWindowCredit_edit").on('close', function () {
        });

        $('#form-Credit_edit').jqxValidator({
            hintType: 'label',
            animationDuration: 500,
            rules: [
            { input: '#payment_value', message: 'Required', action: 'blur', 
            rule: function(input) {
                val = $('#payment_value').val();
                var payment_mode = $('#payment_dropdown').val();
                if(payment_mode == 'bg')
                {
                    return true;
                }
                else
                {
                    return (val == '' || val == null || val == 0) ? false: true; 
                }
            } 
        }] 
    });

        $("#jqxCredit_editSubmitButton").on('click', function () {
         var validationResult = function (isValid) {
            if (isValid) {
             save_Credit_edit();
         }
     };
     $('#form-Credit_edit').jqxValidator('validate', validationResult);

 });
        $("#jqxCredit_editCancelButton").on('click', function () {
            $('#jqxPopupWindowCredit_edit').jqxWindow('close');
        });

         // Cancel Approval
         $("#jqxPopupWindowCredit_cancel").jqxWindow({
            theme: theme,
            width: '40%',
            maxWidth: '40%',
            height: '50%',
            maxHeight: '50%',
            isModal: true,
            autoOpen: false,
            modalOpacity: 0.7,
            showCollapseButton: false
        });

         $("#jqxPopupWindowCredit_cancel").on('close', function () {
         });
         $("#jqxCredit_cancelSubmitButton").on('click', function () {
             save_Credit_cancel();
         });

         $("#jqxCredit_cancelCancelButton").on('click', function () {
            $('#jqxPopupWindowCredit_cancel').jqxWindow('close');
        });

         // Credit Reject
            $("#jqxPopupWindowCredit_reject").jqxWindow({
                theme: theme,
                width: '40%',
                maxWidth: '40%',
                height: '50%',
                maxHeight: '50%',
                isModal: true,
                autoOpen: false,
                modalOpacity: 0.7,
                showCollapseButton: false
            });

            $("#jqxPopupWindowCredit_reject").on('close', function () {
            });

            $('#form-Credit_reject').jqxValidator({
                hintType: 'label',
                animationDuration: 500,
                rules: [
                { input: '#reject_remarks', message: 'Required', action: 'blur', 
                rule: function(input) {
                    val = $('#reject_remarks').val();
                    return (val == '' || val == null || val == 0) ? false: true; 
                } 
            }] 
            });

            $("#jqxCredit_rejectSubmitButton").on('click', function () {
               var validationResult = function (isValid) {
                if (isValid) {
                   save_Credit_reject();
               }
            };
            $('#form-Credit_reject').jqxValidator('validate', validationResult);

            });
            $("#jqxCredit_rejectCancelButton").on('click', function () {
                $('#jqxPopupWindowCredit_reject').jqxWindow('close');
            });

         function credit_approve(index){
            var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
            if (row) 
            {
                $('#approval_order_id').val(row.order_id);
                $('#payment_details').html(row.payment_detail);
                if(row.credit_control_age >= 1)
                {
                    $('#remarks_credit').show();
                    $('#remarks').prop('hidden',false);
                }
                openPopupWindow('jqxPopupWindowCredit_approve', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
            }
        }

        function credit_edit(index){
            var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
            if (row) 
            {
                $('#edit_order_id').val(row.order_id);
                $('#payment_value').val(row.associated_value_payment);
                $('#payment_dropdown').val(row.payment_method);
                openPopupWindow('jqxPopupWindowCredit_edit', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
            }
        }

        function credit_reject(index){
            var row = $("#jqxGridDealer_order").jqxGrid('getrowdata', index);
            if (row) 
            {
                $('#reject_order_id').val(row.order_id);
                $('#reject_dealer_id').val(row.dealer_id);
                $('#payment_details_reject').html(row.payment_detail);
                openPopupWindow('jqxPopupWindowCredit_reject', '<?php echo lang("general_edit") . "&nbsp;" . $header; ?>');
            }
        } 

        function save_Credit_approve()
        {
            var data = $("#form-Credit_approve").serialize();
            $('#jqxPopupWindowCredit_approve').block({
                message: '<span>Processing your request. Please be patient.</span>',
                css: {
                    width: '75%',
                    border: 'none',
                    padding: '50px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .7,
                    color: '#fff',
                    cursor: 'wait'
                },
            });

            $.ajax({
                type: "POST",
                url: '<?php echo site_url("admin/dealer_orders/save_credit_approve"); ?>',
                data: data,
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.success == true) {
                        $('#jqxGridDealer_order').jqxGrid('updatebounddata');
                        $('#jqxPopupWindowCredit_approve').jqxWindow('close');
                    }                   
                    $('#jqxPopupWindowCredit_approve').unblock();
                }
            });
        }

        function save_Credit_edit()
        {
            var data = $("#form-Credit_edit").serialize();
            $('#jqxPopupWindowCredit_edit').block({
                message: '<span>Processing your request. Please be patient.</span>',
                css: {
                    width: '75%',
                    border: 'none',
                    padding: '50px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .7,
                    color: '#fff',
                    cursor: 'wait'
                },
            });

            $.ajax({
                type: "POST",
                url: '<?php echo site_url("admin/dealer_orders/save_credit_edit"); ?>',
                data: data,
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.success == true) {
                        $('#jqxGridDealer_order').jqxGrid('updatebounddata');
                        $('#jqxPopupWindowCredit_edit').jqxWindow('close');
                    }                   
                    $('#jqxPopupWindowCredit_edit').unblock();
                }
            });
        }

        function save_Credit_reject()
        {
            var data = $("#form-Credit_reject").serialize();
            $('#jqxPopupWindowCredit_reject').block({
                message: '<span>Processing your request. Please be patient.</span>',
                css: {
                    width: '75%',
                    border: 'none',
                    padding: '50px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .7,
                    color: '#fff',
                    cursor: 'wait'
                },
            });

            $.ajax({
                type: "POST",
                url: '<?php echo site_url("admin/dealer_orders/save_credit_reject"); ?>',
                data: data,
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.success == true) {
                        $('#jqxGridDealer_order').jqxGrid('updatebounddata');
                        $('#jqxPopupWindowCredit_reject').jqxWindow('close');
                    }                   
                    $('#jqxPopupWindowCredit_reject').unblock();
                }
            });
        }
    </script>