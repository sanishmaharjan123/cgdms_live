	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id="jqxPicking_list"></div>
			</div><!-- /.col -->
		</div>
	</section>

	<div id="jqxPopupWindow_Picking_list">
		<div class='jqxExpander-custom-div'>
			<span class='popup_title'>Picklist Items</span>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="jqxGrid_Picklist_items"></div>
			</div>
		</div>

	</div>

	<script language="javascript" type="text/javascript">


		$(function(){	

			var picklist_Datasource =
			{
				datatype: "json",
				datafields: [
				{ name: 'order_no', type: 'number' },			
				{ name: 'order', type: 'string' },
				{ name: 'dealer_name', type: 'string' },
				{ name: 'pick_count', type: 'number' },
				{ name: 'dealer_id', type: 'number' },
				{ name: 'picklist_no', type: 'number' },
				{ name: 'picklist_format', type: 'string' },
				{ name: 'billed_status', type: 'string' },

				],
				url: '<?php echo site_url("admin/sparepart_orders/picking_list_json"); ?>',
				pagesize: defaultPageSize,
				root: 'rows',
				id : 'id',
				cache: true,
				pager: function (pagenum, pagesize, oldpagenum) {
				},
				beforeprocessing: function (data) {
					picklist_Datasource.totalrecords = data.total;
				},
				filter: function () {
					$("#jqxPicking_list").jqxGrid('updatebounddata', 'filter');
				},
				sort: function () {
					$("#jqxPicking_list").jqxGrid('updatebounddata', 'sort');
				},
				processdata: function(data) {
				}
			};
			
			$("#jqxPicking_list").jqxGrid({
				theme: theme,
				width: '100%',
				height: gridHeight,
				source: picklist_Datasource,
				altrows: true,
				pageable: true,
				sortable: true,
				rowsheight: 30,
				columnsheight:30,
				showfilterrow: true,
				filterable: true,
				columnsresize: true,
				autoshowfiltericon: true,
				columnsreorder: true,
				selectionmode: 'multiplecellsadvanced',
				virtualmode: true,
				enableanimations: false,
				pagesizeoptions: pagesizeoptions,
				showtoolbar: true,
				rendertoolbar: function (toolbar) {
					var container = $("<div style='margin: 5px; height:50px'></div>");
					container.append($('#jqxPicking_listToolbar').html());
					toolbar.append(container);
				},
				columns: [
				{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
				{
					text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
					cellsrenderer: function (index, row, columnfield, value, defaulthtml, columnproperties) {
						var rows = $("#jqxPicking_list").jqxGrid('getrowdata', index);
						var e = '';
						e += '<a href="javascript:void(0)" onclick="list_items(' + index + ')" return false;" title="Picklist Items"><i class="fa fa-outdent" aria-hidden="true"></i> &nbsp';				
						e += '<a href="<?php echo site_url('admin/sparepart_orders/print_pickin_list') ?>/'+rows.dealer_id+'/'+rows.order_no+'/'+rows.pick_count+'" title="Print Picklist" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>';				
						return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
					}
				},	
				{ text: '<?php echo lang("dealer_name"); ?>',datafield: 'dealer_name',width: 300,renderer: gridColumnsRenderer },
				{ text: '<?php echo lang("order"); ?>',datafield: 'order',width: 150,renderer: gridColumnsRenderer },
				{ text: '<?php echo lang("pick_count"); ?>',datafield: 'pick_count',width: 150,renderer: gridColumnsRenderer },
				{ text: '<?php echo lang("picklist_format"); ?>',datafield: 'picklist_format',width: 150,renderer: gridColumnsRenderer },
				{ text: '<?php echo lang("billed_status"); ?>',datafield: 'billed_status',width: 150,renderer: gridColumnsRenderer },
				],
				rendergridrows: function (result) {
					return result.data;
				}
			});
			$("[data-toggle='offcanvas']").click(function(e) {
				e.preventDefault();
				setTimeout(function() {$("#jqxPicking_list").jqxGrid('refresh');}, 500);
			});

	// Dealer Order
	$("#jqxPopupWindow_Picking_list").jqxWindow({
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',
		maxHeight: '75%',
		isModal: true,
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false
	});
});

		function list_items(index)
		{
			var rows = $("#jqxPicking_list").jqxGrid('getrowdata', index);

			var sparepart_Pickinglist =
			{
				datatype: "json",
				datafields: [
				{ name: 'order_no', type: 'number' },			
				{ name: 'order', type: 'string' },
				{ name: 'dealer_name', type: 'string' },
				{ name: 'pick_count', type: 'number' },
				{ name: 'dealer_id', type: 'number' },
				{ name: 'name', type: 'string' },
				{ name: 'part_code', type: 'string' },
				{ name: 'dispatch_quantity', type: 'number' },
				],
				url: '<?php echo site_url("admin/sparepart_orders/picking_list_detail_json")?>',
				data: {dealer_id:rows.dealer_id, order_no:rows.order_no, pick_count: rows.pick_count},
				pagesize: defaultPageSize,
				root: 'rows',
				id : 'id',
				cache: true,
				pager: function (pagenum, pagesize, oldpagenum) {
				},
				beforeprocessing: function (data) {
					sparepart_Pickinglist.totalrecords = data.total;
				},
				filter: function () {

				},
				sort: function () {
					$("#jqxGrid_Picklist_items").jqxGrid('updatebounddata', 'sort');
				},
				processdata: function(data) {
				}
			};

			$("#jqxGrid_Picklist_items").jqxGrid({
				theme: theme,
				width: '100%',
				height: gridHeight,
				source: sparepart_Pickinglist,
				altrows: true,
				pageable: true,
				sortable: true,
				rowsheight: 30,
				columnsheight:30,
				showfilterrow: false,
				filterable: false,
				columnsresize: true,
				autoshowfiltericon: true,
				columnsreorder: true,
				selectionmode: 'none',
				virtualmode: true,
				enableanimations: false,
				pagesizeoptions: pagesizeoptions,
				showtoolbar: true,
				rendertoolbar: function (toolbar) {
					var container = $("<div style='margin: 5px; height:50px'></div>");
					container.append($('#jqxGrid_Picklist_itemsToolbar').html());
					toolbar.append(container);
				},
				columns: [
				{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
				{ text: '<?php echo lang("part_code"); ?>',datafield: 'part_code',width: 200,filterable: true,renderer: gridColumnsRenderer },
				{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 300,filterable: true,renderer: gridColumnsRenderer },
				{ text: '<?php echo lang("dispatch_quantity"); ?>',datafield: 'dispatch_quantity',width: 150,filterable: true,renderer: gridColumnsRenderer },
				],
				rendergridrows: function (result) {
					return result.data;
				}
			});
			openPopupWindow('jqxPopupWindow_Picking_list', 'Picking List Items');
		}

	</script>