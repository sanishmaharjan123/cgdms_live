	<section class="content">
		<section class="content-header "><!-- connectedSortable -->
			<?php echo displayStatus(); ?>
		</section>		
		<div class="box">
			<div class="box billing">
				<!-- <div class="row bill-type">
					<div class="col-md-2"><label>Select Bill Type</label></div>
					<div id="order" class="col-md-2"> ORDER </div>
					<div id="foc" class="col-md-9">  FOC </div>
				</div> -->
				<div class="col-md-12">
					<h2>Billing</h2>
					<div id="order-form">
						<?php echo form_open('', array('id' => 'form-dispatch_list', 'onsubmit' => 'return false')); ?>
						<!-- <div class="row">
							<div class="col-md-2">
								<label>Dealer:</label>
							</div>
							<div class="col-md-10">
								<div id="dealer_list" name="dealer_id"></div>
							</div>							
						</div>
						<div class="row">
							<div class="col-md-2">
								<label>Order Type:</label>
							</div>
							<div class="col-md-10">
								<div id="order_type" name="order_type"></div>												
							</div>							
						</div>
						<div class="row">
							<div class="col-md-2">
								<label>Order No:</label>
							</div>
							<div class="col-md-10">
								<div id="order_list" name="order_no"></div>												
							</div>							
						</div> -->
						<div class="row">
							<div class="col-md-2">
								<label>Picklist No.:</label>
							</div>
							<div class="col-md-10">
								<div id="picklist_no" name="picklist_no"></div>												
							</div>							
						</div>
						<br/>
						<div class="row" id="vor_div" style="display: none;">
							<div class="col-md-2">
								<label>VOR(Add Percentage)</label>
							</div>
							<div class="col-md-10">
								<input type="text" name="vor_percentage" class="text_input">
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-md-2"><label>Barcode Scan</label></div>
							<div class="col-md-6"><input type = "text" class = "form-control" name = "barcode_partcode" id="scan_barcode"></div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
				<div id="foc-form" style="display: none;">
					<div class="row">
						<div class="col-md-1">
							<label>Customer:</label>
						</div>
						<div class="col-md-11">
							<div id="customer_list"></div>
						</div>							
					</div>
					<button id="list_foc_spareparts" class="btn btn-xs btn-flat btn-danger">List Parts</button>
				</div>
			</div>
			<section>				
				<div class="col-md-12">
					<div id="jqxGridPiList"></div>
				</div>
				<div class="box-footer clearfix">
					<div id="foc-bill" style="display: none;"> 
						<form action ="<?php echo site_url('sparepart_orders/generate_excel')?>/foc">
							<input type="hidden" name="customer_id" id='customer_id'> 
							<button type="submit" id="foc-billing-btn" class="btn btn-xs btn-flat btn-danger">Generate FOC Bill</button>
						</form>
					</div>
					<div id="order-bill"">
						<form action ="<?php echo site_url('sparepart_orders/generate_excel')?>/order">
							<input type="hidden" name="dealer_id" id='dealer_id_bill'> 
							<input type="hidden" name="order_no" id='order_no_bill'> 
							<input type="hidden" name="vor_percentage" id="vor_percentage_bill">
							<!-- <div class="row">
								<div class="col-md-2">
									<label for="dispatch_mode">Dispatch Mode</label>
								</div>
								<div class="col-md-10">
									<div class="col-md-1">
										<!-- <div id="radio_dispatch_land">Land</div>
									</div>
									<div class="col-md-11">
										<div id="radio_dispatch_air">Air</div>
									</div>
								</div>
							</div> -->
							<div class="row">
								<div class="col-md-2">
									<label for="discount">Enter Discount</label>		
								</div>
								<div class="col-md-10">
									<input type="text" class="text_input" name="discount_percentage"><br/>
								</div>
							</div>
							<div class="row -->">
								<div class="col-md-2">
									<label for="image_save">Bill Image</label>	
								</div>
								<div class="col-md-10">
									<input type="checkbox"  name="image_save">
								</div>
							</div>
							<button type="submit" id="order-billing-btn" class="btn btn-xs btn-flat btn-danger">Generate Order Bill</button>
						</form>
					</div>
				</div>
			</section>
		</div>
	</section>
<!-- <div id="jqxPopupWindowDispatch_list">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title'><?php echo "Scan Spareparts"//lang('cancel_order');?></span>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php echo form_open('', array('id' => 'form-dispatch_list', 'onsubmit' => 'return false')); ?>
			<input type = "hidden" name = "pi_order_no" id = "pi_order_no"/>
			<input type = "hidden" name = "pi_dealer_id" id = "pi_dealer_id"/>
			<table class="form-table">
				<tr>
					<th colspan="4" style="text-align: center !important;">
						<button type="button" class="btn btn-success btn-lg" id="jqxPi_ConfirmSubmitButton"><?php echo "Confirm"//lang('general_save'); ?></button>
						<button type="button" class="btn btn-default btn-lg" id="jqxPi_ConfirmCancelButton"><?php echo lang('general_cancel'); ?></button>
					</th>
				</tr>
			</table>
			<?php echo form_close(); ?>
		</div>
	</div>
</div> -->

<script language="javascript" type="text/javascript">
	$(function(){
		/*$("#radio_dispatch_land").jqxRadioButton({ width: 250, height: 25});
		$("#radio_dispatch_air").jqxRadioButton({ width: 250, height: 25});
		$('#radio_dispatch_land').on('change', function (event) { 
			var checked = event.args.checked;
			if(checked == true)
			{
				$('#order_dispatch_mode').val('land');			
			}
		});
		$('#radio_dispatch_air').on('change', function (event) { 
			var checked = event.args.checked;
			if(checked == true)
			{
				$('#order_dispatch_mode').val('air');			
			}
		});*/
		var picklistDataSource = {
			url : '<?php echo site_url("admin/sparepart_orders/get_picklist_list"); ?>',
			datatype: 'json',
			datafields: [
			{ name: 'picklist_no', type: 'number' },
			{ name: 'picklist_format', type: 'string' },
			{ name: 'order_type', type: 'string' },
			],
			async: false,
			cache: true
		}

		picklistsDataAdapter = new $.jqx.dataAdapter(picklistDataSource);

		$("#picklist_no").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: picklistsDataAdapter,
			displayMember: "picklist_format",
			valueMember: "picklist_no",
		});

		

		$("#picklist_no").bind('select', function (event) {

			if (!event.args)
				return;

			var picklist_no = $("#picklist_no").jqxComboBox('val');

			var item = $("#picklist_no").jqxComboBox('getItemByValue', picklist_no);
			if(item.originalItem.order_type == 'VOR')
			{
				$('#vor_div').show();
			}
			else
			{
				$('#vor_div').hide();
			}

		});


		

		/*$("#order").jqxRadioButton({ width: 120, height: 25 });
		$("#foc").jqxRadioButton({ width: 120, height: 25 });
		$("#order").bind('change', function (event) {
			var checked = event.args.checked;
			if(checked)
			{
				$('#order-form').show();
				$('#foc-form').hide();
				$('#order-bill').show();
				$('#foc-bill').hide();
			}
		});
		$("#foc").bind('change', function (event) {
			var checked = event.args.checked;
			if(checked)
			{
				$('#foc-form').show();
				$('#order-form').hide();
				$('#foc-bill').show();
				$('#order-bill').hide();
			}
		});
		*/
		var dispatch_ListDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'name', type: 'string' },
			{ name: 'part_code', type: 'string' },
			{ name: 'dispatch_quantity', type: 'number' },
			{ name: 'dealer_price', type: 'number' },
			{ name: 'picklist_no', type: 'number' },
			{ name: 'order_id', type: 'number' },
			],
			url: '<?php echo site_url("admin/sparepart_orders/dispatch_spareparts_list_json"); ?>',
			data : {picklist_no:picklist_no},
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
			},
			beforeprocessing: function (data) {
				dispatch_ListDataSource.totalrecords = data.total;
			},
			filter: function () {
				$("#jqxGridUser").jqxGrid('updatebounddata', 'filter');
			},
			sort: function () {
				$("#jqxGridUser").jqxGrid('updatebounddata', 'sort');
			},
			processdata: function(data) {
			}
		};

		$("#jqxGridPiList").jqxGrid({		
			width: '100%',
			height: gridHeight,
			//source: sparepart_ordersDataSource,
			altrows: true,
			pageable: true,
			sortable: true,
			rowsheight: 30,
			columnsheight:30,
			showfilterrow: true,
			filterable: true,
			columnsresize: true,
			autoshowfiltericon: true,
			columnsreorder: true,
			showstatusbar: true,
			theme:theme,
			editable: true,
			statusbarheight: 30,
			pagesizeoptions: pagesizeoptions,
			showtoolbar: true,
			virtualmode: true,
			showaggregates: true,
			selectionmode: 'singlecell',
			ready: function () {
				var rowsCount = $("#jqxGridPiList").jqxGrid("getrows").length;
				for (var i = 0; i < rowsCount; i++) {
					var currentQuantity = $("#jqxGridPiList").jqxGrid('getcellvalue', i, "dispatched_quantity");
					var currentPrice = $("#jqxGridPiList").jqxGrid('getcellvalue', i, "dealer_price");
					var currentTotal = currentQuantity * currentPrice;
					$("#jqxGridPiList").jqxGrid('setcellvalue', i, "total", currentTotal.toFixed(2));
				}
			},
			columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false,editable:false},

			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width:150,  align: 'center' , hidden:true, cellsalign: 'left',filterable: false,editable:false,renderer: gridColumnsRenderer },										
			{ text: '<?php echo lang("picklist_no"); ?>',datafield: 'picklist_no',width:150,  align: 'center' , hidden:true, cellsalign: 'left',filterable: false,editable:false,renderer: gridColumnsRenderer },										
			{ text: '<?php echo lang("order_id"); ?>',datafield: 'order_id',width:150,  align: 'center' , hidden:true, cellsalign: 'left',filterable: false,editable:false,renderer: gridColumnsRenderer },										
			{ text: '<?php echo lang("part_code"); ?>',datafield: 'part_code',width:150,  align: 'center' , cellsalign: 'left',filterable: false,editable:false,renderer: gridColumnsRenderer },										
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 200,filterable: false,editable:false, align: 'center' , cellsalign: 'left',renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("price"); ?>',datafield: 'dealer_price',width:150, filterable: false,editable:false, align: 'center' , cellsalign: 'right' , cellsformat:'F2', renderer: gridColumnsRenderer},										
			{ text: '<?php echo lang("dispatch_quantity"); ?>',datafield: 'dispatch_quantity',width:200, filterable: false,cellsalign: 'right',renderer: gridColumnsRenderer,aggregates: ['sum'] },
			{
				text: 'Total Amount', datafield: 'total', sortable:false , width:200, filterable:false, align: 'center' , cellsalign: 'right', 
				cellsrenderer: function (index) {
					var row = $("#jqxGridPiList").jqxGrid('getrowdata', index);
					var e = row.dealer_price * row.dispatch_quantity;
					return '<div style="text-align: right; margin-top: 8px;">' + e.toLocaleString('en-IN', {minimumFractionDigits : 2}) + '</div>';
				}
			},	

			],
			rendergridrows: function (result) {
				return result.data;
			}
		});

		$("[data-toggle='offcanvas']").click(function(e) {
			e.preventDefault();
			setTimeout(function() {$("#jqxGridPiList").jqxGrid('refresh');}, 500);
		});

		$(document).on('click','#jqxGridPiListFilterClear', function () { 
			$('#jqxGridPiList').jqxGrid('clearfilters');
		});

	});

	var datarow = new Array();
	$('#list_foc_spareparts').click(function(){
		var customer_id = $('#customer_list').val();
		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/list_foc_spareparts"); ?>',
			data: {customer_id:customer_id},
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success == true) 
				{
					$("#jqxGridPiList").jqxGrid('clear');
					$('#customer_id').val(result.customer_id);
					$.each(result.rows,function(i,v){								
						datarow = {
							'name':v.name,
							'part_code':v.part_code
						};
						$("#jqxGridPiList").jqxGrid('addrow', null, datarow);
					});
				}		
			}
		});
	});

	$('#scan_barcode').keypress(function(e){
		if(e.which == 13) 
		{
			var code = $('#scan_barcode').val();
			var picklist_no = $('#picklist_no').val();
			$('#picklist_no_bill').val(picklist_no);
			$.post('<?php echo site_url('admin/sparepart_orders/get_barcode_values'); ?>',{code:code,picklist_no:picklist_no},function(result){
				if (result.success == true) 
				{
					$("#jqxGridPiList").jqxGrid('clear')

					$.each(result.dispatch_list,function(i,v){								
						datarow = {
							'name':v.name,
							'part_code':v.part_code,
							'dispatch_quantity' : v.dispatch_quantity,
							'picklist_no' : v.picklist_no,
							'order_id' : v.order_id,
							'dealer_price' : v.dealer_price,
							'id' : v.id
						};
						$("#jqxGridPiList").jqxGrid('addrow', null, datarow);
					});
					$('#scan_barcode').val('');
				}
				else
				{
					alert(result.msg);
				}
			},'JSON');
		}
	});

	$('#jqxGridPiList').on('cellvaluechanged', function (event) {
		var rowBoundIndex = event.args.rowindex;
		var rowdata = $('#jqxGridPiList').jqxGrid('getrowdatabyid', rowBoundIndex);

		$.post('<?php echo site_url('admin/sparepart_orders/update_dispatch_quantity') ?>',{id : rowdata.id, dispatch_quantity:rowdata.dispatch_quantity,order_id : rowdata.order_id,picklist_no:rowdata.picklist_no},function(result)
		{
			if(result.success)
			{
			}
			else
			{
				
				$('#jqxGridPiList').jqxGrid('source',dispatch_ListDataSource);
				alert('Quantity Exceeds');
			}

		});

	});



</script>