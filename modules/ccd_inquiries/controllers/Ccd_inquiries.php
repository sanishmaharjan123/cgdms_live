<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Ccd_inquiries
 *
 * Extends the Project_Controller class
 * 
 */

class Ccd_inquiries extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		//control('Ccd Inquiries');

		$this->load->model('ccd_inquiries/ccd_inquiry_model');
        $this->load->model('employees/employee_model');

		$this->lang->load('ccd_inquiries/ccd_inquiry');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('ccd_inquiries');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'ccd_inquiries';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$emp_id = $this->employee_model->find(array('user_id'=>$this->session->userdata('id')),'designation_id');

		$this->ccd_inquiry_model->_table = "view_ccd_inquiry";
		search_params();
		if($emp_id)
		{
			if($emp_id->designation_id == CCD_INQUIRY_GENERATED)
			{
				$this->db->where('source_id',2);
			}
			elseif($emp_id->designation_id == CCD_INQUIRY_WALK_REFERRAL)
			{
				$this->db->where('source_id <>',2);
			}

		}
		$total=$this->ccd_inquiry_model->find_count();
		
		paging('id');
		
		search_params();
		if($emp_id)
		{
			if($emp_id->designation_id == CCD_INQUIRY_GENERATED)
			{
				$this->db->where('source_id',2);
			}
			elseif($emp_id->designation_id == CCD_INQUIRY_WALK_REFERRAL)
			{
				$this->db->where('source_id <>',2);
			}

		}
		$rows=$this->ccd_inquiry_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        $success=$this->ccd_inquiry_model->update($data['id'],$data);

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	$data['id'] = $this->input->post('id');
    	$this->db->select("MAX('call_count') as call_count");
    	$this->db->where('id',$data['id']);
    	$this->db->group_by('id');
    	$max_count = $this->ccd_inquiry_model->find();

    	$data['date_of_call'] = date('Y-m-d');
    	$data['call_status'] = $this->input->post('call_status');
    	$data['date_of_call_np'] = get_nepali_date($data['date_of_call'],'nep');
    	$data['sales_experience'] = $this->input->post('sales_experience');
    	$data['dse_attitude'] = $this->input->post('dse_attitude');
    	$data['dse_knowledge'] = $this->input->post('dse_knowledge');
    	$data['scheme_information'] = $this->input->post('scheme_information');
    	$data['retail_finanace'] = $this->input->post('retail_finanace');
    	$data['offered_test_drive'] = $this->input->post('offered_test_drive');
    	$data['warrenty_policy'] = $this->input->post('warrenty_policy');
    	$data['service_policy'] = $this->input->post('service_policy');
    	$data['remarks'] = $this->input->post('remarks');
    	$data['voc'] = $this->input->post('voc');
    	$data['call_count'] = (($max_count->call_count)+1);

    	return $data;
    }
}