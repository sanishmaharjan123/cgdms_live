<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Estimate_details
 *
 * Extends the Project_Controller class
 * 
 */

class Estimate_details extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Estimate Details');

		$this->load->model('estimate_details/estimate_detail_model');
		$this->lang->load('estimate_details/estimate_detail');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('estimate_details');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'estimate_details';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();

		$this->estimate_detail_model->_table = "view_service_estimate_records";
		if( ! is_admin() ) {
			$this->db->where('dealer_id', $this->dealer_id);
		}
		$total=$this->estimate_detail_model->find_count();

		paging('id');

		search_params();

		if( ! is_admin() ) {
			$this->db->where('dealer_id', $this->dealer_id);
		}
		$rows=$this->estimate_detail_model->findAll();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
		$this->db->trans_begin();

		$data = $this->_get_posted_data(); //Retrive Posted Data
		$jobs_data 	= $this->input->post('jobs_data');
		$parts_data = $this->input->post('parts_data');

		if( ! $this->input->post('data')['id'])
		{
			$estimate_id = $this->estimate_detail_model->insert($data);
		}
		else
		{
			$this->estimate_detail_model->update($data['id'],$data);
			$estimate_id = $data['id'];
		}



		if($jobs_data)
		{
			foreach ($jobs_data as $key => $value) {
				$data = array(
					'job_id'				=>	$value['job_id'],
					'price'					=>	$value['price'],
					'discount'				=>	$value['discount'],
					'total_amount'          =>  $value['total_amount'],
					'customer_voice'			=>	$value['customer_voice'],
					'estimate_id'			=>	$estimate_id,
				);

				$this->estimate_detail_model->_table = "ser_estimate_jobs";
				if( isset($value['id']) ){
					if( $value['id'] ){
						$data['id'] = $value['id'];
						$this->estimate_detail_model->update($data['id'], $data);
					}
				} else {
					$this->estimate_detail_model->insert($data);
				}
			}
		}

		if($parts_data)
		{
			foreach ($parts_data as $key => $value) {
				$data = array(
					'part_id'				=>	$value['part_id'],
					'price'					=>	$value['price'],
					'quantity'				=>	$value['quantity'],
					'discount_percentage'	=>	$value['discount'],
					'final_amount'			=>	$value['final_amount'],
					'estimate_id'			=>	$estimate_id,
				);

				$this->estimate_detail_model->_table = "ser_estimate_parts";
				if(isset($value['id'])) {
					if($value['id']) {
						$data['id'] = $value['id'];
						$this->estimate_detail_model->update($data['id'],$data);
					}
				}
				else {
					$this->estimate_detail_model->insert($data);
				}
			}			
		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$msg = lang('general_failure');
			$success = FALSE;
			echo json_encode(array('success' => $success));
		}
		else
		{
			$this->db->trans_commit();
			$msg = lang('general_success');
			$success = TRUE;
			echo json_encode(array('success' => $success));
		}

	}

	private function _get_posted_data()
	{
		$data=array();
		$post 		= $this->input->post('data');

		if($post['id']) {
			$data['id'] = $post['id'];
		}

		if($post['estimate_doc_no']) {
			$data['estimate_doc_no'] = $post['estimate_doc_no'];
		} else {
			$this->estimate_detail_model->_table = 'ser_estimate_details';
			$this->db->where('dealer_id', $this->dealer_id);
			$estimate_no = $this->estimate_detail_model->find(null,'max(estimate_doc_no)');

			$estimate_no = ++$estimate_no->max;
			$data['estimate_doc_no'] = $estimate_no;
		}

		$data['dealer_id'] = $this->dealer_id;
		$data['jobcard_group'] = $post['jobcard_group'];
		$data['vehicle_register_no'] = $post['vehicle_register_no'];
		$data['chassis_no'] = $post['chassis_no'];
		$data['engine_no'] = $post['engine_no'];
		// $data['model_no'] = $post['model_no'];
		// $data['variant'] = $post['variant'];
		// $data['color'] = $post['color'];
		// $data['ledger_id'] = $post['party_name'];

		$data['total_jobs'] = $post['total_for_jobs'];
		$data['total_parts'] = $post['total_for_parts'];
		

		$data['cash_percent'] = $post['cash_discount_percent'];
		$data['vat_percent'] = $post['vat_percent'];
		$data['net_total'] = $post['net_total'];

		return $data;
	}

	public function get_estimate_jobs() {
		$id = $this->input->get('id');
		$estimate_no = $this->input->get('estimate_id');


		$this->estimate_detail_model->_table = "view_service_estimate_jobs";
		search_params();
		// $this->db->where('dealer_id', $this->dealer_id);
		$this->db->where('estimate_id', $id);
		$rows = $this->estimate_detail_model->findAll();
		$total = count($rows);

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function get_estimate_parts() {
		$id = $this->input->get('id');
		$estimate_no = $this->input->get('estimate_id');


		$this->estimate_detail_model->_table = "view_service_estimate_parts";
		search_params();
		// $this->db->where('dealer_id', $this->dealer_id);
		$this->db->where('estimate_id', $id);
		$rows = $this->estimate_detail_model->findAll();
		$total = count($rows);

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function add_job() {

	}

}