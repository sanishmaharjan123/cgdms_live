<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Spareparts
 *
 * Extends the Project_Controller class
 * 
 */

class Spareparts extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Spareparts');

        $this->load->model('spareparts/sparepart_model');
        $this->lang->load('spareparts/sparepart');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('spareparts');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'spareparts';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->sparepart_model->find_count();
		
		paging('part_code','asc');
		
		search_params();
		
		$rows=$this->sparepart_model->findAll();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->sparepart_model->insert($data);
        }
        else
        {
            $success=$this->sparepart_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['name'] = $this->input->post('name');
		$data['part_code'] = $this->input->post('part_code');
		$data['latest_part_code'] = $this->input->post('latest_part_code');
		$data['alternate_part_code'] = $this->input->post('alternate_part_code');
		$data['dealer_price'] = $this->input->post('dealer_price');
		$data['price'] = $this->input->post('price');
		$data['moq'] = $this->input->post('moq');
		$data['uom'] = $this->input->post('uom');

        return $data;
   }

   /****** for combo box ************/
    public function get_spareparts_combo_json(){
			 $search_name = strtolower($this->input->get('name_startsWith'));
	 		 $where["lower(part_name) LIKE '%{$search_name}%'"] = NULL;

			 $this->sparepart_model->_table = "view_spareparts_all_dealer_stock";
			 if(!is_admin()){
			 	$this->db->where('dealer_id', $this->dealer_id);
			 }
			 $data = $this->sparepart_model->findAll($where, NULL, NULL, NULL, 300);

			 echo json_encode($data);
		 }
   /******** for detail **********/
   public function getDetail(){
   		$where = (int)$this->input->post('id');
   		$data['success'] = FALSE;

   		if($where != '' && is_int($where)){
   			$data = $this->sparepart_model->findBy('id',$where);
   			$data->success = TRUE;
   		}
   		
   		echo json_encode($data);
   }

   public function master_stock_price() {
		// echo "asdf";
		// Display Page
		$data['header'] = lang('spareparts');
		$data['page'] = $this->config->item('template_admin') . "uploads";
		$data['module'] = 'spareparts';
		$this->load->view($this->_container,$data);
	}

	public function uploading_file_select() {

		$config['upload_path'] = './uploads/spareparts_stockprice_import';
		$config['allowed_types'] = 'xlsx|csv|xls';
		$config['max_size'] = "10485760";

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			// $this->load->view('upload_form', $error);
			// redirect();
			print_r($error);
			exit;

		}
		$data = array('upload_data' => $this->upload->data());

		ini_set('memory_limit', '-1');

		$this->load->library('Excel');
		
		$inputFileName = FCPATH . 'uploads/spareparts_stockprice_import/' . $data['upload_data']['file_name'];
		// $inputFileName = FCPATH . 'uploads/spareparts_stockprice_import/asdf.xlsx'; 
		// $inputFileName = FCPATH . 'uploads/spareparts_stockprice_import/master_magh_stock_price.xlsx'; 

		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);
		
		$index = array('sn','part_code','part_name','dealer_price','price');
		$raw_data = array();
		$where_data = array();
		foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
			if($key == '0') {
				$worksheetTitle = $worksheet->getTitle();
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

				$i = 0;
				for ($row = 3; $row <= $highestRow; ++$row) {
					for ($col = 1; $col < $highestColumnIndex; ++$col) {
						$cell = $worksheet->getCellByColumnAndRow($col, $row);
						$val = $cell->getValue();
						if( $index[$col] != "part_name"){
							$raw_data[$i][$index[$col]] = $val;
						}
						if( $index[$col] == "part_code"){
							$where_data[] = $val;
						}
					}
					$i++;
				}

			}
		}

		$this->db->trans_begin();

		$this->db->update_batch('mst_spareparts', $raw_data, 'part_code');

		if($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			$success = FALSE;
		}
		else
		{
			$this->db->trans_commit();
			$success = TRUE;
			echo "DONE";
		}


		exit;
	}
   

}