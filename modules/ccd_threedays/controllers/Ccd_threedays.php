<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Ccd_threedays
 *
 * Extends the Project_Controller class
 * 
 */

class Ccd_threedays extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		control('Ccd Threedays');

		$this->load->model('ccd_threedays/ccd_threeday_model');
		$this->lang->load('ccd_threedays/ccd_threeday');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('ccd_threedays');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'ccd_threedays';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->ccd_threeday_model->_table = "view_ccd_threedays";

		search_params();
		
		$total=$this->ccd_threeday_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->ccd_threeday_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->ccd_threeday_model->insert($data);
        }
        else
        {
        	$success=$this->ccd_threeday_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	$data['id'] = $this->input->post('id');
    	$this->db->select("MAX('call_count') as call_count");
    	$this->db->where('id',$data['id']);
    	$this->db->group_by('id');
    	$max_count = $this->ccd_threeday_model->find();
    	$data['call_count'] = (($max_count->call_count)+1);
    	$data['call_status'] = $this->input->post('call_status');
    	$data['date_of_call'] = date('Y-m-d');
    	$data['date_of_call_np'] = get_nepali_date($data['date_of_call'],'nep');
    	$data['delivered_on_time'] = $this->input->post('delivered_on_time');
    	$data['cleanliness_of_car'] = $this->input->post('cleanliness_of_car');
    	$data['basic_features'] = $this->input->post('basic_features');
    	$data['owner_manual'] = $this->input->post('owner_manual');
    	$data['service_coupon'] = $this->input->post('service_coupon');
    	$data['warrenty_card'] = $this->input->post('warrenty_card');
    	$data['delivery_sheet'] = $this->input->post('delivery_sheet');
    	$data['ccd_details'] = $this->input->post('ccd_details');
    	$data['remarks'] = $this->input->post('remarks');
    	$data['voc'] = $this->input->post('voc');

    	return $data;
    }
}