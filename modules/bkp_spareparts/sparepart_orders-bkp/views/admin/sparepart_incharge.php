
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('sparepart_orders'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('sparepart_orders'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div class="col-md-12">
					<button class="btn btn-warning btn-sm btn-flat" style="float: right;" onclick="OpenReceipt()">Add Receipt</button>
				</div>				
				<div id='jqxTabs'>
					<ul style='margin-left: 20px;'>
						<li>Spare-Parts Orders</li>
						<li>Perfoma Invoice </li>
					</ul>
					<div>
						<div class="row">
							<div class="col-md-12">
								<div id="jqxGridSparepart_order"></div>
							</div>
						</div>
					</div>
					<div>
						<div class="row">
							<div class="col-md-12">
								<div id="jqxGridPI_indexed"></div>

							</div>
						</div>
					</div>
				</div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->	
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowSparepart_order">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-sparepart_orders', 'onsubmit' => 'return false')); ?>
		<input type = "text" name="barcode" id="barcode">
		<input type = "text" name="dispatch_quantity" id="dispatch_quantity" placeholder="Enter Quantity">
		<input type = "hidden" name="stocklist_id[]" id="stocklist_id">
		<table class = "form-table table table-striped" id="dispatchitemlist">
			<thead>
				<tr><th>Name</th><th>Part Code</th><th>Quantity</th><th>Price</th></tr>
			</thead>
			<tbody>			
			</tbody>
			<tfoot>	
				<tr>
					<td><div id="error_dispatchitemlist"></div></td>
				</tr>			
				<tr>
					<th colspan="2">
						<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxSparepart_orderSubmitButton"><?php echo lang('general_save'); ?></button>
						<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxSparepart_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
					</th>
				</tr>
			</tfoot>

		</table>
		<?php echo form_close(); ?>
	</div>
</div>
<div id='leftlog_jqxwindow' style="display: none;">
	<div>BackLog Inventories</div>
	<div>
		<h2>Items out of stock</h2>
		<div class="row">
			<div class="col-md-12">
				<div id="jqxGridLeftLogList"></div>
			</div>
		</div>
	</div>
</div>
<div id="jqxPopupWindowreceipt">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title">Receipt</span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-receipt', 'onsubmit' => 'return false')); ?>	

		<table class="form-table">
			<tr><td><label for="dealer"><?php echo lang('dealer_name') ?></label></td><td><div class="jqxdealer_list" name="dealer_id" style="float: left;margin-right: 20px; margin-top: -5px"></div></td></tr>
			<tr><td><label for="receipt_no">Receipt No:</label></td><td><input class="text_input" type="text" name="receipt_no" id="receipt_no"></td></tr>
			<tr><td><label for="amount">Amount :</label></td><td><input class="text_input" type="text" name="debit_amount" id="amount"></td></tr>
			<tr>
				<td>
					<label for="receipt_image">Receipt Image :</label>
				</td>
				<td>
					<input type="text" class="text_input" name="image_name" id="payment_image_name" hidden>
					<div id="payment_jqxFileUpload"></div>
					<div id="payment_output"></div>
					<button type="button" class="btn btn-default waves-effect" id="payment_change_image" title="Change Image" style="display:none"><i class="fa fa-exchange" aria-hidden="true"></i>Change Image</button> 
				</td>
			</tr>			
			<tr>
				<th>
					<button type="submit" class="btn btn-success btn-flat  btn-md" id="jqxreceiptSubmitButton"><?php echo lang('general_save'); ?></button>
					<button type="button" class="btn btn-default  btn-flat btn-md" id="jqxreceiptCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>	
<div id="jqxPopupWindowPi_Confirm">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title'><?php echo lang('confirm_pi');?></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' => 'form-confirm_pi', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "pi_order_no" id = "pi_order_no"/>
		<table class="form-table">
			<tr>
				<th colspan="4" style="text-align: center !important;">
					<button type="button" class="btn btn-success btn-lg" id="jqxPi_ConfirmSubmitButton"><?php echo "Confirm"//lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-lg" id="jqxPi_ConfirmCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>

<div id="jqxPopupWindowOrder_cancel">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title'><?php echo lang('cancel_order');?></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' => 'form-cancel_order', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "order_no" id = "order_no"/>
		<table class="form-table">
			<tr>
				<th colspan="4" style="text-align: center !important;">
					<button type="button" class="btn btn-danger btn-lg" id="jqxOrder_CancelSubmitButton"><?php echo "Confirm"//lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-lg" id="jqxOrder_CancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="jqxPopupWindowUnavailable_list">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title'><?php echo "Unavailable Spareparts List"//lang('cancel_order');?></span>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="unavailable">				
			</div>
			<div id="error_msg" style="display: none;">All Spareparts are available</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function () {
		$('#jqxTabs').jqxTabs({ width: 'auto', height: 'auto' });
	});
</script>
<script language="javascript" type="text/javascript">
	var barcode_array = new Array();
	var stocklist_id_array = new Array();
	var required_qty_array = new Array();

	$(function(){

		$("#jqxPopupWindowPi_Confirm").jqxWindow({ 
			theme: theme,
			width: '20%',
			maxWidth: '20%',
			height: '15%',  
			maxHeight: '15%',  
			isModal: true, 
			autoOpen: false,
			modalOpacity: 0.7,
			showCollapseButton: false 
		});

		$("#jqxPopupWindowPi_Confirm").on('close', function () {
		});

		$("#jqxPi_ConfirmCancelButton").on('click', function () {
			$('#jqxPopupWindowPi_Confirm').jqxWindow('close');
		});
		$("#jqxPi_ConfirmSubmitButton").on('click', function () {
			save_Confirm_Pi();
		});


		$("#jqxPopupWindowUnavailable_list").jqxWindow({ 
			theme: theme,
			width: '40%',
			maxWidth: '40%',
			height: '40%',  
			maxHeight: '40%',  
			isModal: true, 
			autoOpen: false,
			modalOpacity: 0.7,
			showCollapseButton: false 
		});

		$("#jqxPopupWindowUnavailable_list").on('close', function () {
		});

		$("#jqxPopupWindowOrder_cancel").jqxWindow({ 
			theme: theme,
			width: '20%',
			maxWidth: '20%',
			height: '15%',  
			maxHeight: '15%',  
			isModal: true, 
			autoOpen: false,
			modalOpacity: 0.7,
			showCollapseButton: false 
		});

		$("#jqxPopupWindowOrder_cancel").on('close', function () {
		});

		$("#jqxOrder_CancelButton").on('click', function () {
			$('#jqxPopupWindowOrder_cancel').jqxWindow('close');
		});
		$("#jqxOrder_CancelSubmitButton").on('click', function () {
			save_Order_Cancel();
		});


		var sparepart_orders_group_DataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },			
			{ name: 'sparepart_id', type: 'number' },
			{ name: 'order_quantity', type: 'number' },
			{ name: 'name', type: 'string' },
			{ name: 'part_code', type: 'string' },
			{ name: 'dealer_name', type: 'string' },
			{ name: 'order_no', type: 'number' },

			],
			url: '<?php echo site_url("admin/sparepart_orders/incharge_json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	sparepart_orders_group_DataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSparepart_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSparepart_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSparepart_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: sparepart_orders_group_DataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSparepart_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var row = $("#jqxGridSparepart_order").jqxGrid('getrowdata', index);
				var e = '<a href="<?php echo site_url('sparepart_orders/order_list')?>/'+row.order_no+'/1" return false;" title="List" target="_blank"><i class="fa fa-list"></i></a> &nbsp';
				e += '<a href="javascript:void(0)" onclick="pi_confirm('+ row.order_no+')" title="Approve"><i class="fa fa-check"></i></a>&nbsp';
				e += '<a href="javascript:void(0)" onclick="cancel_order('+ row.order_no+')" title="Decline"><i class="fa fa-remove"></i></a>&nbsp';
				e += '<a href="javascript:void(0)" onclick="unavailable_list('+ row.order_no+')" title="Unavailable List"><i class="fa fa-list-alt"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},		
		{ text: '<?php echo lang("dealer_name"); ?>',datafield: 'dealer_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("order_no"); ?>',datafield: 'order_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});
	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSparepart_order").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSparepart_orderFilterClear', function () { 
		$('#jqxGridSparepart_order').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridSparepart_orderInsert', function () { 
		openPopupWindow('jqxPopupWindowSparepart_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

	// initialize the popup window
	$("#jqxPopupWindowSparepart_order").jqxWindow({ 
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowSparepart_order").on('close', function () {
		reset_form_sparepart_orders();
	});

	$("#jqxSparepart_orderCancelButton").on('click', function () {
		reset_form_sparepart_orders();
		$('#jqxPopupWindowSparepart_order').jqxWindow('close');
	});


	//debit receipt modal
	$('#payment_jqxFileUpload').jqxFileUpload({
		width: 300,
		accept: 'image/*',
		uploadUrl: '<?php  echo site_url('admin/sparepart_orders/file_upload')?>',
		fileInputName: 'fileToUpload'
	});

	$('#payment_jqxFileUpload').on('uploadEnd', function (event) {
		var args = event.args;
		var fileName = args.file;
		var serverResponse = args.response;

		$('#payment_image_name').val(fileName);     
		$('#payment_output').html("<img src='"+base_url+"/uploads/debit_receipt/"+fileName+"' max-width='800px' height='200px'>");   
		$('#payment_jqxFileUpload').hide();
		$("#payment_change_image").show();

	});

	$("#jqxPopupWindowreceipt").jqxWindow({
		theme: theme,
		width: '50%',
		maxWidth: '50%',
		height: '90%',
		maxHeight: '90%',
		isModal: true,
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false
	});

	$("#jqxPopupWindowreceipt").on('close', function () {
	});

	$("#jqxreceiptCancelButton").on('click', function () {
		$('#jqxPopupWindowreceipt').jqxWindow('close');
	});		

	$("#jqxreceiptSubmitButton").on('click', function () {
		save_Receipt();
	});


	$("#jqxSparepart_orderSubmitButton").on('click', function () {
		saveSparepart_orderRecord();      
	});

    // var source =
    // {
    // 	datatype: "json",
    // 	datafields: [
    // 	{ name: 'id' },
    // 	{ name: 'name' }
    // 	],
    // 	url: '<?php echo site_url('admin/sparepart_orders/get_dealer_list') ?>',
    // 	async: false
    // };
    // var dataAdapter = new $.jqx.dataAdapter(source);
    // $(".jqxdealer_list").jqxComboBox({ selectedIndex: 0, 
    // 	source: dataAdapter, 
    // 	displayMember: "name", 
    // 	valueMember: "id", 
    // 	width: 200, 
    // 	height: 25,

    // });

});

function editSparepart_orderRecord(index){
	var row =  $("#jqxGridPI_indexed").jqxGrid('getrowdata', index);
	console.log(row);
	if (row) {
		$('#sparepart_orders_id').val(row.id);	
		$('#dealer_id').val(row.created_by);		
		openPopupWindow('jqxPopupWindowSparepart_order', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
	}
}

function saveSparepart_orderRecord(){
	var data = $("#form-sparepart_orders").serialize();
	
	/*$('#jqxPopupWindowSparepart_order').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});*/

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/sparepart_orders/save_dispatch_order"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_sparepart_orders();
				$('#jqxGridSparepart_order').jqxGrid('updatebounddata');
				$('#jqxPopupWindowSparepart_order').jqxWindow('close');
			}
			$('#jqxPopupWindowSparepart_order').unblock();
		}
	});
}

function reset_form_sparepart_orders(){
	$('#sparepart_orders_id').val('');
	$('#form-sparepart_orders')[0].reset();
}

$('#barcode').keypress(function(e){
	if(e.which == 13)
	{
		var barcode = $('#barcode').val();
		if($.inArray(barcode,barcode_array) < 0){
			$.ajax({
				type: "POST",
				url: '<?php echo site_url("admin/sparepart_orders/list_item_json"); ?>',
				data: {barcode:barcode},
				success: function (result) {				
					var result = eval('('+result+')');			
					if (result.success == true) {
						barcode_array.push(result.stocklist.barcode);
						
						stocklist_id_array.push(result.stocklist.stock_id);
						// required_qty_array.push(result.stocklist.order_quantity);
						//PI
						//Dispatch Qty
						$('#stocklist_id').val(stocklist_id_array);

						$('#dispatchitemlist tbody').append('<tr><td>'+result.stocklist.name+'</td><td>'+result.stocklist.part_code+'</td><td>'+result.stocklist.price+'</td><td>'+result.stocklist.location+'</td><td>'+result.stocklist.barcode+'</td></tr>');
						$('#barcode').val('');
					}								
				}
			});
		}
		else
		{
			$('#error_dispatchitemlist').html('');
			$('#error_dispatchitemlist').append('Error: Do not scan the same item twice');

		}
	}
});

</script>
<script type="text/javascript">
	$(function(){
		var PI_indexed_DataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'dealer_id', type: 'number' },			
			{ name: 'dealer_name', type: 'string' },
			{ name: 'proforma_invoice_id', type: 'number' },

			],
			url: '<?php echo site_url("admin/sparepart_orders/pi_indexed_json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	PI_indexed_DataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridPI_indexed").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridPI_indexed").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridPI_indexed").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: PI_indexed_DataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPI_indexedToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{ 
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center',	cellsrenderer: function (index,b,c,d,e,data) {
				// console.log(data);
				// var e = '<a href="javascript:void(0)" onclick="editSparepart_orderRecord(' + data.proforma_invoice_id + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				var e = '<a target="_blank" href="<?php echo site_url('admin/sparepart_orders/dispatch_list'); ?>/'+ data.proforma_invoice_id + '"  title="Dispatch"><i class="fa fa-edit"></i></a>&nbsp';
				e += '<a target="_blank" href="<?php echo site_url('admin/sparepart_orders/dispatch_left_log'); ?>/'+ data.proforma_invoice_id +'" title="BackLogs"> <i class="fa fa-list-alt"></i> </a> &nbsp';
				var f = '<a target="_blank" href="<?php echo site_url('admin/sparepart_orders/generate_picking_list/');?>/'+data.proforma_invoice_id+'"><i class="fa fa-th-list"></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + f + '</div>';
				
			}
		},
		{ text: '<?php echo lang("dealer_name"); ?>',datafield: 'dealer_name',width: 150,filterable: true,renderer: gridColumnsRenderer },		
		{ text: '<?php echo "PI ID(tochange)"; ?>',datafield: 'proforma_invoice_id',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		} 
	});
});

	function leftlog_jqxwindow(PI)
	{
		$('#jqxGridLeftLogList').jqxGrid('clear');
		$("#leftlog_jqxwindow ").jqxWindow({
			height:'800',
			width: '80%',
			maxWidth: 1200,
			isModal: true,
			modalOpacity: 0.7,
			autoOpen: false,
		});

		$('#leftlog_jqxwindow').jqxWindow('open');
	}
</script>

<script type="text/javascript">
	function OpenReceipt() 
	{
		openPopupWindow('jqxPopupWindowreceipt', '<?php echo "Delivery Sheet" . "&nbsp;" .  $header; ?>');
	}

	$("#payment_change_image").click(function(){
		var filename = $('#image_name').val();
		$.post("<?php echo site_url('admin/sparepart_orders/upload_delete')?>", {filename:filename}, function(){
			$("#payment_change_image").hide();
			$('#payment_jqxFileUpload').show();
			$('#payment_image_name').text('');
			$('#payment_output').hide();			
		});
	});

	function save_Receipt()
	{
		var data = $("#form-receipt").serialize();

		$('#jqxPopupWindowreceipt').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/save_receipt"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {
					reset_form_receipt();
					$('#jqxPopupWindowreceipt').jqxWindow('close');						
				}
				$('#jqxPopupWindowreceipt').unblock();
				location.reload();

			}
		});
		function reset_form_receipt()
		{
			$('#vehicle_process_id').val('');
			$('#form-receipt')[0].reset();
		}
	}

	function cancel_order(order_no)
	{
		$('#order_no').val(order_no);
		openPopupWindow('jqxPopupWindowOrder_cancel', '<?php echo lang("cancel_order")  . "&nbsp;" .  $header; ?>');
	}

	function save_Order_Cancel()
	{
		var data = $("#form-cancel_order").serialize();

		$('#jqxPopupWindowOrder_cancel').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/cancel_order"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {				
					$('#jqxGridSparepart_order').jqxGrid('updatebounddata');
					$('#jqxPopupWindowOrder_cancel').jqxWindow('close');
				}
				$('#jqxPopupWindowOrder_cancel').unblock();
			}
		});
	}

	function pi_confirm(order_no)
	{	
		$('#pi_order_no').val(order_no);
		openPopupWindow('jqxPopupWindowPi_Confirm', '<?php echo lang("confirm_pi")  . "&nbsp;" .  $header; ?>');
	}

	function save_Confirm_Pi(){
		var data = $("#form-confirm_pi").serialize();

		$('#jqxPopupWindowPi_Confirm').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/save_pi"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {				
					$('#jqxPopupWindowPi_Confirm').jqxWindow('close');
				}
				$('#jqxPopupWindowPi_Confirm').unblock();
			}
		});
	}

	function unavailable_list(order_no)
	{
		openPopupWindow('jqxPopupWindowUnavailable_list', '<?php echo lang("confirm_pi")  . "&nbsp;" .  $header; ?>');
		$('#unavailable').html('');
		$('#error_msg').hide();
		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/generate_unavailable_list"); ?>',
			data: {order_no : order_no},
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {				
					$.each(result.unavailable_parts,function(i,v)
					{
						$('#unavailable').append('<div class="list">'+v+'</div>')
					});
				}
				else
				{
					$('#error_msg').show();
				}
			}
		});
	}
</script>