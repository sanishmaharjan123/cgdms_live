<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">
		<h3>Order No. :<?php echo $order_no; ?></h3>
		<!-- row -->
		<a href="<?php echo site_url('admin/sparepart_orders/generate_pi')."/$order_no/1"?>" class="btn btn-xs btn-success btn-flat">Excel PI</a>
		<a href="<?php echo site_url('admin/sparepart_orders/generate_pi')."/$order_no/2"?>" class="btn btn-xs btn-success btn-flat" target="_blank">PDF PI</a>
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>		
				<div id="jqxOrder_list"></div>
			</div><!-- /.col -->
		</div>
	</section>
</div>


<script language="javascript" type="text/javascript">

	$(function(){

		var sparepart_ordersDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },			
			{ name: 'sparepart_id', type: 'number' },
			{ name: 'order_quantity', type: 'number' },
			{ name: 'name', type: 'string' },
			{ name: 'part_code', type: 'string' },
			{ name: 'dealer_name', type: 'string' },
			{ name: 'order_no', type: 'number' },
			{ name: 'pi_status', type: 'string' },			

			],
			url: '<?php echo site_url("admin/sparepart_orders/order_list_json")."/$order_no";?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	sparepart_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxOrder_list").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxOrder_list").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	

	$("#jqxOrder_list").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: sparepart_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxOrder_listToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("part_code"); ?>',datafield: 'part_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("quantity"); ?>',datafield: 'order_quantity',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("pi_status"); ?>',datafield: 'pi_status',width: 150,filterable: true,renderer: gridColumnsRenderer },
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxOrder_list").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxOrder_listFilterClear', function () { 
		$('#jqxOrder_list').jqxGrid('clearfilters');
	});
});


</script>