<span style="text-align: center;"><h3><U>Picking List</U></h3></span>
<span style="text-align: center;"><h4><U>Order No.:<?php echo $order_no;?></U></h4></span>
<table cellspacing="0" cellpadding="0" style="width: 100%; border-collapse: collapse;" border="1">
	<tr>
		<th style="width:50px;padding-top:7px; padding-left: 10px;">S.N.</th>
		<th style="width:150px;padding-top:7px; padding-left: 10px;">Part Name</th>
		<th style="width:150px;padding-top:7px; padding-left: 10px;">Part Code</th>
		<th style="width:150px;padding-top:7px; padding-left: 10px;">location</th>
		<th style="width:100px;padding-top:7px; padding-left: 10px;">quantity</th>	
	</tr>
	<?php $total = 0; ?>
	<?php foreach ($rows as $key => $value): ?>
		<?php $left_stock = $value->quantity - $value->dispatched_quantity ?>
		<tr> 
			<td  style="padding-top:7px;padding-left:10px"><?php echo $key + 1;?></td>
			<td  style="padding-top:7px;padding-left:10px;"><?php echo $value->name;?></td>
			<td  style="padding-top:7px;padding-left:10px;" ><?php echo $value->part_code;?></td>
			<td  style="padding-top:7px;padding-left:10px;"><?php echo $value->location;?></td>
			<td  style="padding-top:7px;padding-left:10px;">
				<?php
				if($value->order_quantity <= $left_stock)
				{					
				  	$actual_qty = $value->order_quantity;
				} 
				else
				{
					$actual_qty =  $left_stock;
				}
				echo $actual_qty;
				?>
			</td>      
		</tr>
		<?php  
		$total += $actual_qty;
		?>
	<?php endforeach; ?>
	<tr><td colspan="4" style="text-align:right">Total Quantity</td><td style="padding-left: 10px"><?php echo $total;?></td></tr>
</table>