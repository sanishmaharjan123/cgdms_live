<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/*
* Rename the file to Sparepart_order.php
* and Define Module Library Function (if any)
*/


/* End of file Sparepart_order.php */
/* Location: ./modules/Sparepart_order/libraries/Sparepart_order.php */

class Sparepart_order{
	public $CI;

	public function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->model('sparepart_orders/sparepart_order_model');
		$this->CI->load->model('sparepart_stocks/sparepart_stock_model');
		$this->CI->load->model('spareparts/sparepart_model');
		$this->CI->load->model('spareparts_dealers/spareparts_dealer_model');
		$this->CI->load->model('document_counts/document_count_model');
		$this->CI->load->helper(array('project'));
	}

	public function sparepart_list_json()
	{
		$sparepart = $this->CI->sparepart_model->findAll(NULL,array('id','part_code','name'),NULL,NULL,100);
		array_unshift($sparepart, array('id' => '0', 'part_code' => 'Select Porduct'));
		return $sparepart;
	}

	public function dealer_list_json()
	{
		$dealer_list = $this->CI->spareparts_dealer_model->findAll(NULL,array('id','name'));
		array_unshift($dealer_list, array('id' => '0', 'name' => 'Select Dealer'));
		return $dealer_list;
	}

	public function generate_proforma_invoice($order_no,$doc_count,$export_type)
	{

		$this->CI->sparepart_order_model->_table = 'view_spareparts_dealer_order';
		$result = $this->CI->sparepart_order_model->findAll(array('order_no'=>$order_no));	
		if(!empty($result))			
		{		
			$dealer_id = $result[0]->dealer_id;

			$total_new_amount= 0;
			foreach ($result as $new_value) {
				$total_new_amount += $new_value->price * $new_value->order_quantity;
				$pi_check = $new_value->pi_generated;			
			}

			$this->CI->dealer_credit_model->_table = 'view_credit_policy';
			$credit_check = $this->CI->dealer_credit_model->findAll(array('dealer_id'=>$dealer_id));

			$total_credit = 0;
			$credit_policy = 0;
			foreach ($credit_check as  $value) 
			{
				if($value->cr_dr == 'credit') 
				{
					$total_credit += $value->amount;
				}
				if($value->cr_dr == 'debit')
				{
					$total_credit -= $value->amount;
				}
				$total_new_credit = $total_credit + $total_new_amount;
				$credit_policy = $value->credit_policy;
			}


			if($credit_policy > $total_new_credit)
			{
				if($pi_check == 0)
				{					
					$this->CI->sparepart_order_model->_table = 'spareparts_sparepart_order';
					foreach ($result as $value) {
						$data['id'] = $value->id;
						$data['pi_generated'] = 1;
						$data['proforma_invoice_id'] = ($doc_count->proforma_invoice)+1;
						$success = $this->CI->sparepart_order_model->update($data['id'],$data);
					}

					if($success)
					{
						$this->CI->document_count_model->unsubscribe('before_update', 'audit_log_update');
						$count['id'] = $doc_count->id;
						$count['proforma_invoice'] = ($doc_count->proforma_invoice) + 1;
						$count['updated_at'] = date('Y-m-d H:i:s');
						$this->CI->document_count_model->update($count['id'],$count);										
					}
				}


				if($export_type == 1)
				{
					$this->CI->load->library('Excel');
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->getActiveSheet()->setTitle('Proforma Invoice');
					$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
					$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
					$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->mergeCells('A3:F3');
					$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getStyle('A5:F10')->getFont()->setBold(true);

					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1','PROFORMA INVOICE')
					->setCellValue('A2','Shree Himalayan Enterprises Pvt. Ltd.')
					->setCellValue('A3','Spare Parts Logistic - Satungal')
					->setCellValue('B5','PI No:')
					->setCellValue('C5','HEPL-0262')
					->setCellValue('E5','Order recv.')
					->setCellValue('F5', date('Y-m-d'))
					->setCellValue('B6', 'Dealer:')
					->setCellValue('C6', 'Ait Dhobighat')
					->setCellValue('E6', 'PI issue date')
					->setCellValue('F6', date('Y-m-d'))
					->setCellValue('B7', 'Address:')
					->setCellValue('E7', 'Effective Date')
					->setCellValue('F7', date('Y-m-d'))
					->setCellValue('E8', 'Ord. Type')
					->setCellValue('A10', 'S.N.')
					->setCellValue('B10', 'SUP. PART NO.')
					->setCellValue('C10', 'DESCRIPTION')
					->setCellValue('D10', 'QTY')
					->setCellValue('E10', 'RATE')
					->setCellValue('F10', 'TOTAL');

					$row = 11;
					$col = 0;            
					foreach($result as $key => $values) 
					{			
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $key+1);
						$col++;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->part_code);
						$col++;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->name);
						$col++;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->order_quantity);
						$col++;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->price);
						$col++; 

						$objPHPExcel->getActiveSheet()
						->setCellValue(
							'F'.$row,
							'=(D'.$row.'*E'.$row.')'
							);		

						$col = 0;
						$row++;

					}
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':C'.$row);
					$objPHPExcel->getActiveSheet()

					->setCellValue('A'.$row, 'Total of pi')
					->setCellValue(
						'D'.$row,
						'=SUM(D11:D'.($row-1).')'
						)
					->setCellValue(
						'F'.$row,
						'=SUM(F11:F'.($row-1).')'
						)
					->setCellValue('A'.($row+2), 'Note: Above mentioned rate is excluding the vat amount.');
					$styleArray = array(
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN
								)
							)
						);
					$objPHPExcel->getActiveSheet()->getStyle(
						'A10:F'.($row)
						)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('A'.($row+2).':F'.($row+2))->getFont()->setBold(true);

					$objPHPExcel->getActiveSheet()->getStyle('E11:E'.($row-1))
					->getNumberFormat()
					->setFormatCode('###,###,###.00');

					header("Pragma: public");
					header("Expires: 0");
					header("Content-Type: application/force-download");
					header("Content-Disposition: attachment;filename=Proforma.xls");
					header("Content-Transfer-Encoding: binary ");
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					ob_end_clean();
					$objWriter->save('php://output');
				}
				if($export_type == 2)					
				{
					$this->CI->load->library('html2pdf');
					$data['rows'] = $result;
					$content=$this->CI->load->view('admin/proforma_invoice',$data,TRUE);        
					$file_name = "proforma_invoice.pdf";

					$this->CI->html2pdf->WriteHTML($content);
					$path='uploads/spareparts_pi/';
					$this->CI->html2pdf->Output($path.$file_name); 
				}
			}
			else
			{
				flashMsg('error', 'Credit Limit Exceeds.');     
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
		else
		{
			flashMsg('error', 'PI Already Generated.');     
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	
	public function pi_confirm($order_no)
	{
		$result = $this->CI->sparepart_order_model->findAll(array('order_no'=>$order_no));
		foreach($result as $value)
		{
			$data['id'] = $value->id;
			$data['pi_confirmed'] = 1;
			$success = $this->CI->sparepart_order_model->update($data['id'],$data);
		}
		return $success;	
	}

	public function jqxupload()
	{        
		$target_dir = $_SERVER['DOCUMENT_ROOT']."uploads/debit_receipt/";     
		@mkdir($target_dir,0755,true);
		$target_file = $target_dir. basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		if(isset($_POST["submit"])) 
		{
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} 
			else 
			{
				echo "File is not an image.";
				$uploadOk = 0;
			}
		}
		if (file_exists($target_file)) 
		{
			echo "Sorry, file already exists.";
			$uploadOk = 0;
		}
		if ($_FILES["fileToUpload"]["size"] > 500000) 
		{
			echo "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) 
		{
			echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.";
		} else {
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) 
			{
				echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
			} else 
			{
				echo "Sorry, there was an error uploading your file.";
			}
		}
	}
}