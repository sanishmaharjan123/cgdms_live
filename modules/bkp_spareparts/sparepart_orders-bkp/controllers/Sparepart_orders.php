<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Sparepart_orders
*
* Extends the Project_Controller class
* 
*/

class Sparepart_orders extends Project_Controller
{
    public function __construct()
    {
        parent::__construct();

        control('Sparepart Orders');

        $this->load->model('sparepart_orders/sparepart_order_model');
        $this->load->model('dispatch_spareparts/dispatch_sparepart_model');
        $this->load->model('sparepart_stocks/sparepart_stock_model');
        $this->load->model('spareparts/sparepart_model');
        $this->lang->load('sparepart_orders/sparepart_order');
        $this->load->library('sparepart_orders/sparepart_order');    
        $this->load->model('dealer_credits/dealer_credit_model');

        $this->load->model('order_unavailables/order_unavailable_model');
        $this->load->model('picklists/picklist_model');
        $this->load->model('foc_documents/foc_document_model');
        $this->load->model('foc_accessoreis_partcodes/foc_accessoreis_partcode_model');
        $this->load->model('vehicle_processes/vehicle_process_model');


    }


    public function index()
    {
        $data['header'] = lang('sparepart_orders');
        $data['page'] = $this->config->item('template_admin') . "tab_index";
        $data['module'] = 'sparepart_orders';

        $this->load->view($this->_container,$data);
    }    

    public function sparepart_incharge()
    {
        $data['header'] = lang('sparepart_orders');
        $data['page'] = $this->config->item('template_admin') . "sparepart_incharge";
        $data['module'] = 'sparepart_orders';
        $this->load->view($this->_container,$data);
    }

    public function json()
    {
        $this->sparepart_order_model->_table = 'view_spareparts_order';
        search_params();

        $total=$this->sparepart_order_model->find_count();

        paging('id');
        
        search_params();
        $rows=$this->sparepart_order_model->findAll();
        
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

    public function pi_generated_json()
    {
        $dealer_id = $this->_sparepartdealer->id; 
        $this->sparepart_order_model->_table = 'view_spareparts_order';
        search_params();

        $total=$this->sparepart_order_model->find_count(array('pi_generated'=>1,'pi_confirmed'=>0,'dealer_id'=>$dealer_id));

        paging('id');

        search_params();
        $rows=$this->sparepart_order_model->findAll(array('pi_generated'=>1,'pi_confirmed'=>0,'dealer_id'=>$dealer_id));
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }
    public function incharge_json()
    {
        $this->sparepart_order_model->_table = 'view_grouped_spareparts_order';

        search_params();
        $total=$this->sparepart_order_model->find_count(array('order_cancel'=>0));
        paging('order_no');

        search_params();
        $rows=$this->sparepart_order_model->findAll(array('order_cancel'=>0));
        
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

    public function dealer_order_json()
    {
        $this->sparepart_order_model->_table = 'view_grouped_spareparts_order';

        search_params();
        $total=$this->sparepart_order_model->find_count(array('order_cancel'=>0,'dealer_id'=>$this->_sparepartdealer->id));
        paging('order_no');

        search_params();
        $rows=$this->sparepart_order_model->findAll(array('order_cancel'=>0,'dealer_id'=>$this->_sparepartdealer->id));

        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

    public function order_list_json($order_no = NULL)
    {
        $this->sparepart_order_model->_table = 'view_spareparts_order';
        search_params();

        $total=$this->sparepart_order_model->find_count(array('order_no'=>$order_no));
        paging('order_no');

        search_params();

        $rows=$this->sparepart_order_model->findAll(array('order_no'=>$order_no));
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }
    
    public function pi_indexed_json()
    {
        $fields = 'proforma_invoice_id,dealer_name,dealer_id,order_no';
        $this->sparepart_order_model->_table = 'view_spareparts_order';

        search_params();
        $this->db->group_by($fields);
        $total=$this->sparepart_order_model->find_all(array('pi_generated'=>1,'pi_confirmed'=>1),$fields);
        $total = count($total);

        paging('dealer_id');

        search_params();
        $this->db->group_by($fields);
        $rows=$this->sparepart_order_model->find_all(array('pi_generated'=>1, 'pi_confirmed'=>1),$fields);
        // echo $this->db->last_query();

        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

   /* public function save()
    {

        $data=$this->_get_posted_data(); 

        $this->dealer_credit_model->_table = 'view_credit_policy';
        $credit_check = $this->dealer_credit_model->findAll(array('dealer_id'=>$data['dealer_id']));

        $total_credit = 0;
        $credit_policy = 0;
        foreach ($credit_check as  $value) 
        {
            if($value->cr_dr == 'credit') 
            {
                $total_credit += $value->amount;
            }
            if($value->cr_dr == 'debit')
            {
                $total_credit -= $value->amount;
            }
            $credit_policy = $value->credit_policy;
        }

        if(!$total_credit > $credit_policy)
        {
            if(!$this->input->post('id'))
            {
                $success=$this->sparepart_order_model->insert($data);
            }
            else
            {
                $success=$this->sparepart_order_model->update($data['id'],$data);
            }
        }
        else
        {
            $success = FALSE;
            $msg=lang('excess_credit');
            echo json_encode(array('msg'=>$msg,'success'=>$success));
            exit;
        }

        if($success)
        {
            $success = TRUE;
            $msg=lang('general_success');
        }
        else
        {
            $success = FALSE;
            $msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
        $data=array();
        if($this->input->post('id')) {
            $data['id'] = $this->input->post('id');
        }
        $data['sparepart_id']   = $this->input->post('product_id');
        $data['order_quantity'] = $this->input->post('quantity');
        $data['dealer_id']      = $this->_sparepartdealer->incharge_id;   
        return $data;
    }
*/
    public function dealer_order_import()
    {
        $config['upload_path'] = './uploads/dealer_order';
        $config['allowed_types'] = 'xlsx|csv|xls';
        $config['max_size'] = 100000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $file = FCPATH . 'uploads/dealer_order/' . $data['upload_data']['file_name']; 
        $this->load->library('Excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');        
        $objReader->setReadDataOnly(false);

        $index = array('part_code','order_quantity');
        $raw_data = array();
        $view_data = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
            if ($key == 0) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;

                for ($row = 2; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $raw_data[$row][$index[$col]] = $val;
                    }
                }
            }
        }

        // max order no selected
        $unavailable_part_code = array();
        $this->db->select_max('order_no');
        $result = $this->db->get('spareparts_sparepart_order')->row();
        if(!$result)
        {
            $order_no = 0;
        }
        $order_no = $result->order_no + 1;

        $imported_data = array();
        foreach ($raw_data as $key => $value) {
            $sparepart_id = $this->sparepart_model->find(array('latest_part_code'=>$value['part_code']),array('id','moq'));
            if(!$sparepart_id)
            {
                $unavailable_part_code[] = $value['part_code']; 
            }
            else
            {
                $imported_data[$key]['sparepart_id'] = $sparepart_id->id;
                $imported_data[$key]['created_by'] = $this->session->userdata('id');
                $imported_data[$key]['created_at'] = date("Y-m-d H:i:s");
                if($value['order_quantity'] < $sparepart_id->moq)
                {                    
                    $imported_data[$key]['order_quantity'] = $sparepart_id->moq;
                }                    
                else
                {
                    $imported_data[$key]['order_quantity'] = $value['order_quantity'];
                }
                $imported_data[$key]['dealer_id']      = $this->_sparepartdealer->id;
                $imported_data[$key]['order_no']      = $order_no;
            }    
        } 

        if($unavailable_part_code)
        {            
            $undata['unavailable_parts'] = implode(',',$unavailable_part_code);
            $undata['order_no'] = $order_no;
            $this->order_unavailable_model->insert($undata);
        }

        $this->db->trans_start();
        $this->sparepart_order_model->insert_many($imported_data);
        if ($this->db->trans_status() === FALSE) 
        {
            $this->db->trans_rollback();
        } 
        else 
        {
            $this->db->trans_commit();            
        } 
        $this->db->trans_complete();
    }


    
    public function list_item_json()
    {
        $this->sparepart_stock_model->_table = 'view_spareparts_dealer_order';

        $barcode = strtoupper($this->input->post('barcode'));       
        $proforma_invoice_id = $this->input->post('pi');
        $sparepart_stock_id = $this->input->post('sparepart_stock_id');         
        $where = array(
            'part_code'=>$barcode, 
            'proforma_invoice_id'=>$proforma_invoice_id,
            'pi_generated'          => 1,
            'pi_confirmed'          => 1,
            );
        $stocklist = $this->sparepart_stock_model->find($where);

        if($stocklist == FALSE)
        {
            echo json_encode(array('stocklist'=>$stocklist,'success'=>FALSE));
            exit;
        }

        echo json_encode(array('stocklist'=>$stocklist,'success'=>TRUE));
    }

    public function list_foc_item_json()
    {
        $this->sparepart_stock_model->_table = 'view_sparepart_real_stock';

        $barcode = strtoupper($this->input->post('barcode'));       

        $stocklist = $this->sparepart_stock_model->find(array('name'=>$barcode));

        if($stocklist == FALSE)
        {
            echo json_encode(array('stocklist'=>$stocklist,'success'=>FALSE));
            exit;
        }
        echo json_encode(array('stocklist'=>$stocklist,'success'=>TRUE));
    }


    public function generate_pi($order_no = NULL, $export_type = NULL)
    {
        $doc_count = $this->_document_count[0];             
        $success = $this->sparepart_order->generate_proforma_invoice($order_no,$doc_count,$export_type);
    }

    public function save_pi()
    {
        $order_no = $this->input->post('pi_order_no');
        $success = $this->sparepart_order->pi_confirm($order_no);
        echo json_encode(array('success'=>$success));
    }

    public function get_spareparts_list()
    {

        $list = $this->sparepart_order->sparepart_list_json();
        echo json_encode($list);
    }
    public function get_dealer_list()
    {

        $dealerlist = $this->sparepart_order->dealer_list_json();
        echo json_encode($dealerlist);
    }

    public function save_dispatch_order()
    {
        $post = $this->input->post();
        $post['dispatched_date'] = date("Y-m-d");
        $post['dispatched_date_nepali'] = get_nepali_date(date("Y-m-d"),'string');

        $newstock = array(
            'id' => $post['stock_id'],
            'quantity' => $post['quantity'] - $post['dispatch_quantity'],
            );

        if($newstock['quantity'] < 0){
            $msg = 'Error: No stock.';
            $success = false;
            echo json_encode(array('success'=>$success, 'msg'=>$msg));
            exit;
        }

        $success = $this->sparepart_stock_model->update($newstock['id'],$newstock);

        if($success)
        {
            $data = array(
                'stock_id'              =>  $post['stock_id'],
                'order_id'              =>  $post['order_id'],
                'dispatched_quantity'   =>  $post['dispatch_quantity'],
                'dispatched_date'       =>  $post['dispatched_date'],
                'dispatched_date_nepali'=>  $post['dispatched_date_nepali'],
                'proforma_invoice_id'   =>  $post['proforma_invoice_id'],
                'pick_count'            =>  0
                );

            $success = $this->dispatch_sparepart_model->insert($data);
            if($success)
            {
                $msg = "Successfully Inserted new dispatch item.";
            }
            else
            {
                $msg = "Error: updating.";
            }
        }
        else
        {
            $msg = "Error: Unable to update master stock.";

        }

        echo json_encode(array('success'=>$success, 'msg'=>$msg));

    }

    public function dispatch_list($id = NULL)
    {
        $data['id'] = $id;

        $data['header'] = lang('sparepart_orders');
        $data['page'] = $this->config->item('template_admin') . "dispatch_list";
        $data['module'] = 'sparepart_orders';
        $this->load->view($this->_container,$data);
    }

    public function dispatch_list_json($proforma_invoice_id = NULL)
    {
        $this->dispatch_sparepart_model->_table = 'view_dispatch_spareparts';
        search_params();

        $total=$this->dispatch_sparepart_model->find_count(array('proforma_invoice_id'=>$proforma_invoice_id,'pick_count'=>0));
        paging('id');

        search_params();

        $rows=$this->dispatch_sparepart_model->findAll(array('proforma_invoice_id'=>$proforma_invoice_id,'pick_count'=>0));
        echo json_encode(array('total'=>$total,'rows'=>$rows));
    }

    /*public function generate_bill($proforma_invoice_id = NULL)
    {
        $this->dispatch_sparepart_model->_table = 'view_dispatch_spareparts';
        $rows=$this->dispatch_sparepart_model->findAll(array('proforma_invoice_id'=>$proforma_invoice_id,'pick_count'=> 0));

        //credit part
        $credit = array();
        $credit['amount']= 0;
        foreach ($rows as  $amount) 
        {
            $credit['dealer_id'] = $amount->dealer_id;
            $credit['proforma_invoice_id'] = $amount->proforma_invoice_id;
            $credit['cr_dr'] = 'CREDIT';
            $credit['amount'] += $amount->dispatched_quantity * $amount->price;
            $credit['date'] = date('Y-m-d');
            $credit['date_nepali'] = get_nepali_date(date('Y-m-d'),'nep');
        }

        $success = $this->dealer_credit_model->insert($credit);

        if(empty($rows))
        {

            flashMsg('error', 'Nothing to generate bill.');     
            redirect($_SERVER['HTTP_REFERER']);
        }

        // Update Pick count (Generate Bill Batch)
        $this->dispatch_sparepart_model->_table = 'spareparts_dispatch_spareparts';
        $this->db->select_max('pick_count');
        $pick_count=$this->dispatch_sparepart_model->find(array('proforma_invoice_id'=>$proforma_invoice_id),'');
        $pick_count = $pick_count->pick_count +1;
        $data = array();
        foreach ($rows as $key => $value) {
            $data[] = array(
                'id'    =>  $value->id,
                'pick_count' => $pick_count
                );
        }
        $this->dispatch_sparepart_model->update_batch($data,'id');

        // Generate Excel for Bills
        $this->load->library('Excel');
        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0); 
        $rowCount = 2; 
        foreach($rows as $value){
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->name); 
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->part_code); 
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->order_quantity); 
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->dispatched_quantity); 
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $value->price); 
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->dispatched_date); 
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $value->dispatched_date_nepali); 
            $rowCount++; 
        } 


        header("Pragma: public");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Generate BILL.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');


    }
*/
    public function dispatch_left_log($id = NULL)
    {
        $data['id'] = $id;

        $data['header'] = lang('sparepart_orders');
        $data['page'] = $this->config->item('template_admin') . "leftlogs_spareparts";
        $data['module'] = 'sparepart_orders';
        $this->load->view($this->_container,$data);
    }

    public function dispatch_left_log_json($proforma_invoice_id = NULL)
    {
        $this->dispatch_sparepart_model->_table = 'view_left_spareparts';

        search_params();
        $total=$this->dispatch_sparepart_model->findAll(array('proforma_invoice_id'=>$proforma_invoice_id,'remaining <>'=>0));
        $total = count($total);

        paging('order_id');

        search_params();
        $rows=$this->dispatch_sparepart_model->findAll(array('proforma_invoice_id'=>$proforma_invoice_id,'remaining <>'=>0));

        echo json_encode(array('total'=>$total,'rows'=>$rows));

    }

    public function generate_picking_list($pi_id = NULL)
    {
        $this->load->library('html2pdf');

        $this->sparepart_order_model->_table = 'view_spareparts_dealer_order';
        $data['rows'] = $this->sparepart_order_model->findAll(array('pi_generated'=>1,'pi_confirmed'=>1,'proforma_invoice_id'=>$pi_id,'quantity >'=>0));

        if(empty($data['rows']))
        {
            flashMsg('error', 'No items to generate.');     
            redirect($_SERVER['HTTP_REFERER']);
        }

        foreach ($data['rows'] as  $value) 
        {
            $picklist['order_id'] = $value->id;
            $picklist['order_no'] = $value->order_no;
            $picklist['dealer_id'] = $value->dealer_id;
            $picklist['sparepart_id'] = $value->sparepart_id;
            $picklist['dispatch_quantity'] = $value->order_quantity;
            $picklist['dispatched_date'] = date('Y-m-d');
            $picklist['dispatched_date_nep'] = get_nepali_date(date('Y-m-d'),'nep');
            $success = $this->picklist_model->insert($picklist);
        }

        $data['order_no'] = $picklist['order_no'];
        if($success)
        {
            $content=$this->load->view('admin/picklist',$data,TRUE);        
            $file_name = "picklist.pdf";

            $this->html2pdf->WriteHTML($content);
            $path='uploads/booked_vehicles/';
            $this->html2pdf->Output($path.$file_name); 
        }
    }

    public function file_upload()
    {
        $success =  $this->sparepart_order->jqxupload();
    }

    public function upload_delete(){
        $uploadPath = 'uploads/debit_receipt';
        $filename = $this->input->post('filename');
        @unlink($this->uploadPath . '/' . $filename);
        @unlink($this->uploadthumbpath . '/' . $filename);
    }

    public function save_receipt()
    {
        $data['dealer_id'] = $this->input->post('dealer_id');
        $data['amount'] = $this->input->post('debit_amount');
        $data['receipt_no'] = $this->input->post('receipt_no');
        $data['image_name'] = $this->input->post('image_name');
        $data['cr_dr'] = 'DEBIT';
        $data['date'] = date('Y-m-d');
        $data['date_nepali'] = get_nepali_date(date('Y-m-d'),'nep');

        $success = $this->dealer_credit_model->insert($data);
        if($success)
        {
            echo json_encode(array('success'=>$success));
        }
    }

    public function cancel_order()
    {
        $this->sparepart_order_model->unsubscribe('after_create', 'activity_log_insert');     
        $this->sparepart_order_model->unsubscribe('before_update', 'audit_log_update');
        $data['order_no'] = $this->input->post('order_no');
        $data['order_cancel'] = 1;
        $this->db->where('order_no',$data['order_no']);
        $success = $this->db->update('spareparts_sparepart_order',$data);
        echo json_encode(array('success'=>$success));
    }

    public function generate_unavailable_list()
    {
        $order_no = $this->input->post('order_no');
        $result = $this->order_unavailable_model->find(array('order_no'=>$order_no));
        if($result)
        {
            $unavailable = explode(',', $result->unavailable_parts);
            $success = TRUE;
            echo json_encode(array('success'=>$success,'unavailable_parts'=>$unavailable));        
        }
        else
        {
            $success = FALSE;           
            echo json_encode(array('success'=>$success));        
        }
    }  
    public function get_dealer_order_json() 
    {
        $dealer_id = $this->input->get('dealer_id');

        // $this->vehicle_model->_table = 'view_dms_vehicles';
        $this->db->distinct('order_no');

        $this->db->where('dealer_id', $dealer_id);

        $this->db->group_by('order_no');
        
        $rows=$this->sparepart_order_model->findAll(null, array('order_no'));   

        echo json_encode($rows);
    }  

    public function get_foc_customer_json()
    {
        $this->foc_document_model->_table = "view_foc_dropdown";
        $rows=$this->foc_document_model->findAll(array('billed'=>0), array('customer_id','full_name'));
        echo json_encode($rows);
    }

    public function list_foc_spareparts()
    {
        $this->foc_document_model->_table = "view_foc_details";
        $customer_id = $this->input->post('customer_id');
        $acc_id = $this->foc_document_model->find(array('customer_id'=>$customer_id),array('customer_id','accessories_id'));
        $accessories_id = explode(',', $acc_id->accessories_id);

        foreach ($accessories_id as $value) 
        {
            $rows[] = $this->foc_accessoreis_partcode_model->find(array('id' => $value));
        }

        echo json_encode(array('success'=>TRUE,'rows'=>$rows,'customer_id'=>$acc_id->customer_id));
    }

    public function generate_excel($bill_type = NULL)
    {
        $customer_id = $this->input->get('customer_id');
        $dealer_id = $this->input->get('dealer_id');
        $order_no = $this->input->get('order_no');

        if($bill_type == 'foc')
        {            
            $this->foc_document_model->_table = "view_foc_details";
            $acc_id = $this->foc_document_model->find(array('customer_id'=>$customer_id),array('accessories_id'));
            $accessories_id = explode(',', $acc_id->accessories_id);
            foreach ($accessories_id as $value) 
            {
                $rows[] = $this->foc_accessoreis_partcode_model->find(array('id' => $value));
            }

            if($rows)
            {
                foreach ($rows as $items)
                {
                    $this->sparepart_stock_model->_table = "view_sparepart_real_stock";
                    $sparepart = $this->sparepart_stock_model->find(array('latest_part_code'=>$items->part_code));
                    if($sparepart){                        
                        $new_stock['id'] = $sparepart->id;
                        $new_stock['quantity'] = $sparepart->stock_quantity - 1;
                        $this->sparepart_stock_model->_table = "spareparts_sparepart_stock";
                        $success1 = $this->sparepart_stock_model->update($new_stock['id'],$new_stock); 
                    }
                    else
                    {
                        flashMsg('error', 'Spareparts Not in the Stock');     
                        redirect($_SERVER['HTTP_REFERER']); 
                    }

                    if($success1)
                    {
                        $dispatch_stock['foc_document_id'] = $items->id;
                        $dispatch_stock['stock_id'] = $sparepart->id;
                        $dispatch_stock['dispatched_quantity'] = 1;
                        $dispatch_stock['dispatched_date'] = date('Y-m-d');
                        $dispatch_stock['dispatched_date_nepali'] = get_nepali_date(date('Y-m-d'),'nep');
                        $dispatch_stock['foc'] = 1;
                        $success2 = $this->dispatch_sparepart_model->insert($dispatch_stock); 
                    }
                    if($success2)
                    {
                        $this->foc_document_model->_table = "sales_foc_document";
                        $foc_details = $this->foc_document_model->find(array('customer_id'=>$customer_id));
                        $update_foc['id'] = $foc_details->id;
                        $update_foc['billed'] = 1;
                        $success = $this->foc_document_model->update($update_foc['id'],$update_foc);
                    }
                }
            }
        }
        
        if($bill_type == 'order')
        {
            $this->dispatch_list_model->_table = "view_dispatch_list_spareparts";
            $rows = $this->dispatch_list_model->findAll(array('dealer_id'=>$dealer_id,'order_no'=>$order_no));
        }


        if($success)
        {
            $this->load->library('Excel');

            $objPHPExcel = new PHPExcel(); 
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->SetCellValue('A1','S.N.');
            $objPHPExcel->getActiveSheet()->SetCellValue('B1','Part Code');
            $objPHPExcel->getActiveSheet()->SetCellValue('C1','Name');
            $objPHPExcel->getActiveSheet()->SetCellValue('D1','quantity');

            $row = 2;
            $col = 0;        
            foreach($rows as $key => $values) 
            {           
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $key+1);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->part_code);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->name);
                $col++;
                if($bill_type == 'order')
                {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->dispatch_quantity);
                    $col++;                                       
                }
                else
                {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 1);
                    $col++;
                }
                $col = 0;
                $row++;        
            }

            header("Pragma: public");
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment;filename=Bill.xls");
            header("Content-Transfer-Encoding: binary ");
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
        }
    }

    public function upload_picklist()
    {   

        $dealer_id = $this->input->post('dealer_id');
        $order_no = $this->input->post('order_no');

        $config['upload_path'] = './uploads/dealer_order';
        $config['allowed_types'] = 'xlsx|csv|xls';
        $config['max_size'] = 100000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $file = FCPATH . 'uploads/dealer_order/' . $data['upload_data']['file_name']; 
        $this->load->library('Excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');        
        $objReader->setReadDataOnly(false);

        $index = array('part_code','dispatch_quantity');
        $raw_data = array();
        $view_data = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
            if ($key == 0) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;

                for ($row = 2; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $raw_data[$row][$index[$col]] = $val;
                    }
                }
            }
        }
        foreach ($raw_data as $key => $value) 
        {
            $sparepart_id = $this->sparepart_model->find(array('latest_part_code'=>$value['part_code']));

            $excel_dispatch[$key]['sparepart_id'] = $sparepart_id->id;
            $excel_dispatch[$key]['dealer_id'] = $this->input->post('dealer_id_excel');
            $excel_dispatch[$key]['order_no'] = $this->input->post('order_no_excel');
            $excel_dispatch[$key]['created_by'] = $this->session->userdata('id');
            $excel_dispatch[$key]['created_at'] = date("Y-m-d H:i:s");            
            $excel_dispatch[$key]['dispatch_quantity'] = $value['dispatch_quantity'];
            $excel_dispatch[$key]['part_code'] = $value['part_code'];
        } 

        $this->db->trans_start();
        $this->dispatch_list_model->insert_many($excel_dispatch);
        if ($this->db->trans_status() === FALSE) 
        {
            $this->db->trans_rollback();
            echo 'error';
        } 
        else 
        {
            $this->db->trans_commit();            
            echo 'success';
        } 
        $this->db->trans_complete();
    }

    public function list_order_spareparts()
    {
        $dealer_id = $this->input->post('dealer_id');
        $order_no = $this->input->post('order_no');
        $this->dispatch_list_model->_table = "view_dispatch_list_spareparts";
        $dispatch_list = $this->dispatch_list_model->findAll(array('dealer_id'=>$dealer_id,'order_no'=>$order_no));

        $pick_list = $this->picklist_model->findAll(array('dealer_id'=>$dealer_id,'order_no'=>$order_no));        

        foreach ($pick_list as $pick) 
        {
            foreach ($dispatch_list as $dispatch) 
            {
                if($pick->sparepart_id == $dispatch->sparepart_id)
                {
                    $approved[] = $dispatch;
                }
            }
        }
        echo json_encode(array('success'=>TRUE, 'rows'=>$approved,'dealer_id'=>$dealer_id,'order_no'=>$order_no));
    }

    public function get_customer_details()
    {
        $customer_id = $this->input->post('customer_id');
        $this->vehicle_process_model->_table = "view_vehicle_detail_foc";
        $vehicle_details = $this->vehicle_process_model->find(array('customer_id'=>$customer_id));
        echo json_encode($vehicle_details);
    }
}
