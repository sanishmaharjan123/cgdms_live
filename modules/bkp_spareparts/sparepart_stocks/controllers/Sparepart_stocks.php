<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Sparepart_stocks
 *
 * Extends the Project_Controller class
 * 
 */

class Sparepart_stocks extends Project_Controller
{
	public function __construct()
	{
		parent::__construct();

		//control('Sparepart Stocks');

		$this->load->model('sparepart_stocks/sparepart_stock_model');
		$this->load->model('stock_yards/stock_yard_model');
		$this->lang->load('sparepart_stocks/sparepart_stock');
	}

	public function index()
	{
		// Display Page
		$data['header'] = lang('sparepart_stocks');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'sparepart_stocks';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$id = $this->session->userdata('id');
		$stockyard = $this->stock_yard_model->find(array('incharge_id'=>$id));
		// if(is_sparepart_incharge())
		// {
		// 	$this->sparepart_stock_model->_table = 'view_sparepart_real_stock';			
		// 	$where = array('stockyard_id'=>$stockyard->id);
		// }
		if(is_sparepart_dealer())
		{
			$this->sparepart_stock_model->_table = 'view_sparepart_stock_dealer';
			$where = array('incharge_id'=>$this->session->userdata('id'));			
		}
		else
		{
			$this->sparepart_stock_model->_table = 'view_sparepart_real_stock';
			$where = NULL;
		}

		search_params();
		$total=$this->sparepart_stock_model->find_count($where);
		
		search_params();
		
		$rows=$this->sparepart_stock_model->findAll($where);
		// echo $this->db->last_query();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
        	$success=$this->sparepart_stock_model->insert($data);
        }
        else
        {
        	$success=$this->sparepart_stock_model->update($data['id'],$data);
        }

        if($success)
        {
        	$success = TRUE;
        	$msg=lang('general_success');
        }
        else
        {
        	$success = FALSE;
        	$msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id')) {
    		$data['id'] = $this->input->post('id');
    	}
    	$data['sparepart_id'] = $this->input->post('sparepart_code');
    	$data['location'] = $this->input->post('location');
    	// if(is_sparepart_incharge())
    	// {
    	// 	$id = $this->session->userdata('id');
    	// 	$stockyard = $this->stock_yard_model->find(array('incharge_id'=>$id));
    	// 	$data['stockyard_id'] = $stockyard->id;
    	// }
    	return $data;
    }
}