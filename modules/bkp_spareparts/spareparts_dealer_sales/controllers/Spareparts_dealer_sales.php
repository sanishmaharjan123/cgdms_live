<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Spareparts_dealer_sales
*
* Extends the Project_Controller class
* 
*/

class Spareparts_dealer_sales extends Project_Controller
{
    public function __construct()
    {
        parent::__construct();

        control('Spareparts Dealer Sales');
        $this->load->model('dealer_stocks/dealer_stock_model');
        $this->load->model('spareparts_dealer_sales/spareparts_dealer_sale_model');
        $this->load->model('spareparts_dealersales_lists/spareparts_dealersales_list_model');
        $this->load->model('dealer_stocks/dealer_stock_model');
        $this->load->model('dealer_credits/dealer_credit_model');

        $this->lang->load('spareparts_dealer_sales/spareparts_dealer_sale');
    }

    public function index()
    {
// Display Page
        $data['header'] = lang('spareparts_dealer_sales');
        $data['page'] = $this->config->item('template_admin') . "index";
        $data['module'] = 'spareparts_dealer_sales';
        $this->load->view($this->_container,$data);
    }

    public function json()
    {
        search_params();
        $this->spareparts_dealer_sale_model->_table = "view_spareparts_dealer_sales";

        $total=$this->spareparts_dealer_sale_model->find_count();

        paging('id');

        search_params();

        $rows=$this->spareparts_dealer_sale_model->findAll();

        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }

    public function save()
    {
        $form_data = $this->input->post('formdata');
        $gird_data = $this->input->post('griddata');
        $bill_no = $this->spareparts_dealer_sale_model->find(NULL,'max(bill_no) as bill_no');
        $bill_no = $bill_no->bill_no + 1;

        $form_data['date'] = date('Y-m-d');
        $form_data['nep_date'] = get_nepali_date(date('Y-m-d'),'nep');
        $form_data['bill_no'] = $bill_no;
        // Sales Infromation Insert
        $success = $this->spareparts_dealer_sale_model->insert($form_data);
        if($success)
        {
            foreach ($gird_data as $key => $value) 
            {
                $gird_upload[] = array(
                    'sparepart_id' => $value['sparepart_id'],
                    'quantity' => $value['quantity'],
                    'price' => $value['price'],
                    'dealer_sales_id' => $success,
                    'dispatch_date_nep' => get_nepali_date(date('Y-m-d'),'nep')
                    );

                $spareparts = $this->dealer_stock_model->find(array('sparepart_id'=>$value['sparepart_id'],'dealer_id'=>$this->session->userdata('employee')['dealer_id']));
                $stock_update[] =array(
                    'id'=> $spareparts->id,
                    'quantity' => $spareparts->quantity - $value['quantity']
                    ); 
            }
            // Stock Update
            $this->dealer_stock_model->update_batch($stock_update,'id');            
            // Spareparts list insert
            $this->spareparts_dealersales_list_model->insert_many($gird_upload);

            $credit['dealer_id'] = $form_data['party_id'];
            $credit['cr_dr'] = 'CREDIT';
            $credit['amount'] = $form_data['total_amount'];
            $credit['date'] = date('Y-m-d');
            $credit['date_nepali'] = get_nepali_date(date('Y-m-d'),'nep');
            // Credit Insert
            $this->dealer_credit_model->insert($credit);

        }

        if($success)
        {
            $success = TRUE;
            $msg=lang('general_success');
        }
        else
        {
            $success = FALSE;
            $msg=lang('general_failure');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
        exit;
    }

    public function sparepart_json()
    {
    }

    public function spareparts_list_json()
    {
        $dealer_id = $this->session->userdata('employee')['dealer_id'];
        $this->dealer_stock_model->_table = "view_spareparts_all_dealer_stock";
        $rows = $this->dealer_stock_model->findAll(array('dealer_id'=>$dealer_id),'sparepart_id,latest_part_code,name');
        echo json_encode($rows);
    }
    public function get_item_price()
    {

        $sparepart_id = $this->input->post('id');
        $price = $this->dealer_stock_model->find(array('sparepart_id'=>$sparepart_id),'price');
        echo json_encode($price);
    }

    public function get_dealer_sparepart_list()
    {
        $this->spareparts_dealersales_list_model->_table = "view_spareparts_dealersales_list";
        $rows = $this->spareparts_dealersales_list_model->findAll(array('dealer_sales_id'=>$this->input->post('id')));
        echo json_encode($rows);
    }

    public function check_stock_quantity()
    {
        $sparepart_id = $this->input->post('sparepart_id');
        $quantity = $this->input->post('quantity');
        $spareparts = $this->dealer_stock_model->find(array('sparepart_id'=>$sparepart_id,'dealer_id'=>$this->session->userdata('employee')['dealer_id']));
        if($quantity > $spareparts->quantity)
        {
            $msg = 'Quantity Exceeds';
            $success = FALSE;
        }   
        else
        {
            $msg = 'Success';
            $success = TRUE;
        }
        echo json_encode(array('success'=>$success,'msg'=>$msg));
    }
}