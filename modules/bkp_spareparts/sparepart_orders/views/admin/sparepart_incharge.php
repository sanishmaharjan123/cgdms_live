
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('sparepart_orders'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('sparepart_orders'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<div id="error_pi" class="alert alert-danger" style="display: none;">
					<span>Dealer Has Not Confirmed PI Yet</span>
				</div>
			</div>
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div class="col-md-12">
					<button class="btn btn-warning btn-sm btn-flat" style="float: right;" onclick="OpenReceipt()">Add Receipt</button>
				</div>				
				<div id='jqxTabs'>
					<ul style='margin-left: 20px;'>
						<li>Spare-Parts Orders</li>
						<li>Perfoma Invoice </li>
					</ul>
					<div>
						<div class="row">
							<div class="col-md-12">
								<div id="jqxGridSparepart_order"></div>
							</div>
						</div>
					</div>
					<div>
						<div class="row">
							<div class="col-md-12">
								<div id="jqxGridPI_indexed"></div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->	
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowSparepart_order">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-sparepart_orders', 'onsubmit' => 'return false')); ?>
		<input type = "text" name="barcode" id="barcode">
		<input type = "text" name="dispatch_quantity" id="dispatch_quantity" placeholder="Enter Quantity">
		<input type = "hidden" name="stocklist_id[]" id="stocklist_id">
		<table class = "form-table table table-striped" id="dispatchitemlist">
			<thead>
				<tr><th>Name</th><th>Part Code</th><th>Quantity</th><th>Price</th></tr>
			</thead>
			<tbody>			
			</tbody>
			<tfoot>	
				<tr>
					<td><div id="error_dispatchitemlist"></div></td>
				</tr>			
				<tr>
					<th colspan="2">
						<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxSparepart_orderSubmitButton"><?php echo lang('general_save'); ?></button>
						<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxSparepart_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
					</th>
				</tr>
			</tfoot>

		</table>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="jqxPopupWindowdispatch_list">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title">Upload Picklist</span>
	</div>
	<div class="form_fields_area">
		<span><h4>Upload Picklist File</h4></span>
		<form action="<?php echo site_url('admin/sparepart_orders/upload_picklist') ?>" id="order_form" method="post" enctype="multipart/form-data">		
			<input type="hidden" name="dealer_id_excel" id="dealer_id_excel">
			<input type="hidden" name="order_no_excel" id="order_no_excel">
			<input type="hidden" name="order_type_excel" id="order_type_excel">
			<input type="file" name="userfile" style="float: left;">
			<button type="submit">Import</button>
		</form>				
	</div>
</div>	
<div id="jqxPopupWindowreceipt">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title">Receipt</span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-receipt', 'onsubmit' => 'return false')); ?>	

		<table class="form-table">
			<tr>
				<td><label for="dealer"><?php echo lang('dealer_name') ?></label></td>
				<td><div id="jqxdealer_list_receipt" name="dealer_id" style="float: left;margin-right: 20px; margin-top: -5px"></div></td>
			</tr>
			<tr>
				<td><label for="order_type"> Order Type: </label></td>
				<td><div id="order_type" name="order_type"></div></td>
			</tr>
			<tr>
				<td><label for="order_no"><?php echo lang('order_no') ?></label></td>
				<td><div id="jqx_dealer_order_list" name="order_no" style="float: left;margin-right: 20px; margin-top: -5px"></div></td>
			</tr>
			<tr>
				<td><label for="receipt_no">Receipt No:</label></td>
				<td><input class="text_input" type="text" name="receipt_no" id="receipt_no"></td>
			</tr>
			<tr>
				<td><label for="amount">Amount :</label></td>
				<td><input class="text_input" type="text" name="debit_amount" id="amount"></td>
			</tr>						
			<tr>
				<th>
					<button type="submit" class="btn btn-success btn-flat  btn-md" id="jqxreceiptSubmitButton"><?php echo lang('general_save'); ?></button>
					<button type="button" class="btn btn-default  btn-flat btn-md" id="jqxreceiptCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>	
<div id="jqxPopupWindowPi_Confirm">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title'><?php echo lang('confirm_pi');?></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' => 'form-confirm_pi', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "pi_order_no" id = "pi_order_no"/>
		<input type = "hidden" name = "pi_dealer_id" id = "pi_dealer_id"/>
		<table class="form-table">
			<tr>
				<th colspan="4" style="text-align: center !important;">
					<button type="button" class="btn btn-success btn-lg" id="jqxPi_ConfirmSubmitButton"><?php echo "Confirm"//lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-lg" id="jqxPi_ConfirmCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>

<div id="jqxPopupWindowOrder_cancel">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title'><?php echo lang('cancel_order');?></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' => 'form-cancel_order', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "order_no" id = "order_no"/>
		<table class="form-table">
			<tr>
				<th colspan="4" style="text-align: center !important;">
					<button type="button" class="btn btn-danger btn-lg" id="jqxOrder_CancelSubmitButton"><?php echo "Confirm"//lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-lg" id="jqxOrder_CancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="jqxPopupWindowUnavailable_list">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title'><?php echo "Unavailable Spareparts List"//lang('cancel_order');?></span>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="unavailable">				
			</div>
			<div id="error_msg" style="display: none;">All Spareparts are available</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function () {
		$('#jqxTabs').jqxTabs({ width: 'auto', height: 'auto' });
	});
</script>
<script language="javascript" type="text/javascript">
	var barcode_array = new Array();
	var stocklist_id_array = new Array();
	var required_qty_array = new Array();

	$(function(){

		var dealer_listSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id' },
			{ name: 'name' }
			],
			url: '<?php echo site_url('admin/sparepart_orders/get_dealer_list') ?>',
			async: false
		};
		var dealer_listdataAdapter = new $.jqx.dataAdapter(dealer_listSource);
		$("#jqxdealer_list_receipt").jqxComboBox({ 
			selectedIndex: 0, 
			source: dealer_listdataAdapter, 
			displayMember: "name", 
			valueMember: "id", 
			width: 200, 
			height: 25,

		});

		$("#jqxdealer_list_receipt").bind('select', function (event) {

			if (!event.args)
				return;

			var OrderTypesource = [
			"Select Order Type",
			"STOCK",
			"VOR", 
			];
			$("#order_type").jqxComboBox({ 
				source: OrderTypesource, 
				selectedIndex: 0, 
				width: 195, 
				height: 25 
			});
		});

		$("#order_type").bind('select', function (event) {

			if (!event.args)
				return;

			dealer_id = $("#jqxdealer_list_receipt").jqxComboBox('val');
			order_type = $("#order_type").jqxComboBox('val');

			var order_listDataSource  = {
				url : '<?php echo site_url("admin/sparepart_orders/get_dealer_order_json"); ?>',
				datatype: 'json',
				datafields: [
				{ name: 'order_no', type: 'number' },
				],
				data: {
					dealer_id: dealer_id,
					order_type: order_type
				},
				async: false,
				cache: true
			}

			order_listDataAdapter = new $.jqx.dataAdapter(order_listDataSource, {autoBind: false});

			$("#jqx_dealer_order_list").jqxComboBox({
				theme: theme,
				width: 195,
				height: 25,
				selectionMode: 'dropDownList',
				autoComplete: true,
				searchMode: 'containsignorecase',
				source: order_listDataAdapter,
				displayMember: "order_no",
				valueMember: "order_no",
			});
		});

		$("#jqxPopupWindowPi_Confirm").jqxWindow({ 
			theme: theme,
			width: '20%',
			maxWidth: '20%',
			height: '15%',  
			maxHeight: '15%',  
			isModal: true, 
			autoOpen: false,
			modalOpacity: 0.7,
			showCollapseButton: false 
		});

		$("#jqxPopupWindowPi_Confirm").on('close', function () {
		});

		$("#jqxPi_ConfirmCancelButton").on('click', function () {
			$('#jqxPopupWindowPi_Confirm').jqxWindow('close');
		});
		$("#jqxPi_ConfirmSubmitButton").on('click', function () {
			save_Confirm_Pi();
		});


		$("#jqxPopupWindowUnavailable_list").jqxWindow({ 
			theme: theme,
			width: '40%',
			maxWidth: '40%',
			height: '40%',  
			maxHeight: '40%',  
			isModal: true, 
			autoOpen: false,
			modalOpacity: 0.7,
			showCollapseButton: false 
		});

		$("#jqxPopupWindowUnavailable_list").on('close', function () {
		});

		$("#jqxPopupWindowOrder_cancel").jqxWindow({ 
			theme: theme,
			width: '20%',
			maxWidth: '20%',
			height: '15%',  
			maxHeight: '15%',  
			isModal: true, 
			autoOpen: false,
			modalOpacity: 0.7,
			showCollapseButton: false 
		});

		$("#jqxPopupWindowOrder_cancel").on('close', function () {
		});

		$("#jqxOrder_CancelButton").on('click', function () {
			$('#jqxPopupWindowOrder_cancel').jqxWindow('close');
		});
		$("#jqxOrder_CancelSubmitButton").on('click', function () {
			save_Order_Cancel();
		});


		var sparepart_orders_group_DataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },			
			{ name: 'sparepart_id', type: 'number' },
			{ name: 'order_quantity', type: 'number' },
			{ name: 'name', type: 'string' },
			{ name: 'part_code', type: 'string' },
			{ name: 'dealer_name', type: 'string' },
			{ name: 'order_no', type: 'number' },
			{ name: 'dealer_id', type: 'number' },
			{ name: 'order_concat', type: 'string' },

			],
			url: '<?php echo site_url("admin/sparepart_orders/incharge_json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	sparepart_orders_group_DataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridSparepart_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridSparepart_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridSparepart_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: sparepart_orders_group_DataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridSparepart_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var row = $("#jqxGridSparepart_order").jqxGrid('getrowdata', index);
				var e = '<a href="<?php echo site_url('admin/sparepart_orders/order_list')?>/'+row.order_no+'/'+row.dealer_id+'/1" return false;" title="Order List" target="_blank"><i class="fa fa-list"></i></a> &nbsp';
				 //if(row.pi_confirmed == 0)
				 //{
					e += '<a href="javascript:void(0)" onclick="pi_confirm('+ row.order_no+','+row.dealer_id+')" title="PI Approve"><i class="fa fa-check"></i></a>&nbsp';
					e += '<a href="javascript:void(0)" onclick="cancel_order('+ row.order_no+')" title="PI Decline"><i class="fa fa-remove"></i></a>&nbsp';
				 //}
				e += '<a href="javascript:void(0)" onclick="unavailable_list('+ row.order_no+','+row.dealer_id+')" title="Unavailable List"><i class="fa fa-list-alt"></i></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			}
		},		
		{ text: '<?php echo lang("dealer_name"); ?>',datafield: 'dealer_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("order_no"); ?>',datafield: 'order_concat',width: 150,filterable: true,renderer: gridColumnsRenderer },
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});
	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridSparepart_order").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridSparepart_orderFilterClear', function () { 
		$('#jqxGridSparepart_order').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridSparepart_orderInsert', function () { 
		openPopupWindow('jqxPopupWindowSparepart_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

	// initialize the popup window
	$("#jqxPopupWindowSparepart_order").jqxWindow({ 
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowSparepart_order").on('close', function () {
		reset_form_sparepart_orders();
	});

	$("#jqxSparepart_orderCancelButton").on('click', function () {
		reset_form_sparepart_orders();
		$('#jqxPopupWindowSparepart_order').jqxWindow('close');
	});


	//debit receipt modal	
	$("#jqxPopupWindowreceipt").jqxWindow({
		theme: theme,
		width: '50%',
		maxWidth: '50%',
		height: '90%',
		maxHeight: '90%',
		isModal: true,
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false
	});

	$("#jqxPopupWindowreceipt").on('close', function () {
	});

	$("#jqxreceiptCancelButton").on('click', function () {
		$('#jqxPopupWindowreceipt').jqxWindow('close');
	});		

	$("#jqxreceiptSubmitButton").on('click', function () {
		save_Receipt();
	});


	$("#jqxSparepart_orderSubmitButton").on('click', function () {
		saveSparepart_orderRecord();      
	});


});

function editSparepart_orderRecord(index){
	var row =  $("#jqxGridPI_indexed").jqxGrid('getrowdata', index);
	console.log(row);
	if (row) {
		$('#sparepart_orders_id').val(row.id);	
		$('#dealer_id').val(row.created_by);		
		openPopupWindow('jqxPopupWindowSparepart_order', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
	}
}

function saveSparepart_orderRecord(){
	var data = $("#form-sparepart_orders").serialize();
	
	/*$('#jqxPopupWindowSparepart_order').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});*/

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/sparepart_orders/save_dispatch_order"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_sparepart_orders();
				$('#jqxGridSparepart_order').jqxGrid('updatebounddata');
				$('#jqxPopupWindowSparepart_order').jqxWindow('close');
			}
			$('#jqxPopupWindowSparepart_order').unblock();
		}
	});
}

function reset_form_sparepart_orders(){
	$('#sparepart_orders_id').val('');
	$('#form-sparepart_orders')[0].reset();
}
</script>
<script type="text/javascript">
	$(function(){
		var PI_indexed_DataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'dealer_id', type: 'number' },			
			{ name: 'dealer_name', type: 'string' },
			{ name: 'proforma_invoice_id', type: 'number' },
			{ name: 'order_no', type: 'number' },
			{ name: 'order_concat', type: 'string' },
			{ name: 'order_type', type: 'string' },
			],
			url: '<?php echo site_url("admin/sparepart_orders/pi_indexed_json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	PI_indexed_DataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridPI_indexed").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridPI_indexed").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridPI_indexed").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: PI_indexed_DataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPI_indexedToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{ 
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center',	cellsrenderer: function (index,b,c,d,e,data) {
				// console.log(data);
				// var e = '<a href="javascript:void(0)" onclick="editSparepart_orderRecord(' + data.proforma_invoice_id + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				var e = '<a href="javascript:void(0)" onclick="dispatched_list_upload(' + index + '); return false;" title="Import Barcode File"><i class="fa fa-edit"></i></a>';
				e += '<a target="_blank" href="<?php echo site_url('admin/sparepart_orders/dispatch_left_log'); ?>/'+ data.order_no +'" title="BackLogs"> <i class="fa fa-list-alt"></i> </a> &nbsp';
				var f = '<a target="_blank" href="<?php echo site_url('admin/sparepart_orders/generate_picking_list/');?>/'+data.proforma_invoice_id+'/'+data.dealer_id+'/'+data.order_type+'" title="Picklist"><i class="fa fa-th-list"></a>';
				return '<div style="text-align: center; margin-top: 8px;">' + e + f + '</div>';
				
			}
		},
		{ text: '<?php echo lang("dealer_name"); ?>',datafield: 'dealer_name',width: 150,filterable: true,renderer: gridColumnsRenderer },		
		{ text: '<?php echo lang("order_no"); ?>',datafield: 'order_concat',width: 150,filterable: true,renderer: gridColumnsRenderer },		
		{ text: '<?php echo lang("proforma_invoice_id"); ?>',datafield: 'proforma_invoice_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("order_type"); ?>',datafield: 'order_type',width: 150,filterable: true,renderer: gridColumnsRenderer },
		],
		rendergridrows: function (result) {
			return result.data;
		} 
	});
	$("#jqxPopupWindowdispatch_list").jqxWindow({ 
		theme: theme,
		width: '30%',
		maxWidth: '30%',
		height: '20%',  
		maxHeight: '20%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowdispatch_list").on('close', function () {
	});	
});

function dispatched_list_upload(index)
{
	var row =  $("#jqxGridPI_indexed").jqxGrid('getrowdata', index);
	console.log(row);
	$('#dealer_id_excel').val(row.dealer_id);
	$('#order_no_excel').val(row.order_no);
	$('#order_type_excel').val(row.order_type);
	
	openPopupWindow('jqxPopupWindowdispatch_list', '<?php echo lang("confirm_pi")  . "&nbsp;" .  $header; ?>');	
}
</script>

<script type="text/javascript">
	function OpenReceipt() 
	{
		openPopupWindow('jqxPopupWindowreceipt', '<?php echo "Delivery Sheet" . "&nbsp;" .  $header; ?>');
	}

	function save_Receipt()
	{
		var data = $("#form-receipt").serialize();

		$('#jqxPopupWindowreceipt').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/save_receipt"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {
					reset_form_receipt();
					$('#jqxPopupWindowreceipt').jqxWindow('close');						
				}
				$('#jqxPopupWindowreceipt').unblock();
				location.reload();

			}
		});
		function reset_form_receipt()
		{
			$('#vehicle_process_id').val('');
			$('#form-receipt')[0].reset();
		}
	}

	function cancel_order(order_no)
	{
		$('#order_no').val(order_no);
		openPopupWindow('jqxPopupWindowOrder_cancel', '<?php echo lang("cancel_order")  . "&nbsp;" .  $header; ?>');
	}

	function save_Order_Cancel()
	{
		var data = $("#form-cancel_order").serialize();

		$('#jqxPopupWindowOrder_cancel').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/cancel_order"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {				
					$('#jqxGridSparepart_order').jqxGrid('updatebounddata');
					$('#jqxPopupWindowOrder_cancel').jqxWindow('close');
				}
				$('#jqxPopupWindowOrder_cancel').unblock();
			}
		});
	}

	function pi_confirm(order_no,dealer_id)
	{	
		$('#pi_order_no').val(order_no);
		$('#pi_dealer_id').val(dealer_id);
		openPopupWindow('jqxPopupWindowPi_Confirm', '<?php echo lang("confirm_pi")  . "&nbsp;" .  $header; ?>');
	}

	function save_Confirm_Pi(){
		var data = $("#form-confirm_pi").serialize();

		$('#jqxPopupWindowPi_Confirm').block({ 
			message: '<span>Processing your request. Please be patient.</span>',
			css: { 
				width                   : '75%',
				border                  : 'none', 
				padding                 : '50px', 
				backgroundColor         : '#000', 
				'-webkit-border-radius' : '10px', 
				'-moz-border-radius'    : '10px', 
				opacity                 : .7, 
				color                   : '#fff',
				cursor                  : 'wait' 
			}, 
		});

		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/save_pi"); ?>',
			data: data,
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {				
					$('#jqxPopupWindowPi_Confirm').jqxWindow('close');
				}
				else
				{
					$('#error_pi').delay(500).fadeIn('normal', function() {
						$(this).delay(1000).fadeOut();
					});
					$('#jqxPopupWindowPi_Confirm').jqxWindow('close');
				}
				$('#jqxPopupWindowPi_Confirm').unblock();
			}
		});
	}

	function unavailable_list(order_no,dealer_id)
	{
		openPopupWindow('jqxPopupWindowUnavailable_list', '<?php echo lang("confirm_pi")  . "&nbsp;" .  $header; ?>');
		$('#unavailable').html('');
		$('#error_msg').hide();
		$.ajax({
			type: "POST",
			url: '<?php echo site_url("admin/sparepart_orders/generate_unavailable_list"); ?>',
			data: {order_no : order_no, dealer_id:dealer_id},
			success: function (result) {
				var result = eval('('+result+')');
				if (result.success) {				
					$.each(result.unavailable_parts,function(i,v)
					{
						$('#unavailable').append('<div class="list">'+v+'</div>')
					});
				}
				else
				{
					$('#error_msg').show();
				}
			}
		});
	}
</script>