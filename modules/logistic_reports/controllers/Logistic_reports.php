<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Logistic Report
*
* Extends the Report_Controller class
* 
*/

class Logistic_reports extends Report_Controller
{
    public function __construct()
    {
        parent::__construct();

//control('CRM Reports');

        $this->lang->load('logistic_reports/logistic_report');
        $this->load->model('nepali_months/nepali_month_model');
        $this->load->model('city_places/city_place_model');
        $this->load->model('msil_orders/msil_order_model');
        $this->load->model('target_records/target_record_model');


    }

    public function index()
    {
        $data['header'] = lang('logistic_reports');
        $data['page'] = $this->config->item('template_admin') . "report_list";
        $data['module'] = 'logistic_reports';
        $data['type']   = 'LOGISTIC REPORT';  
        $this->load->view($this->_container,$data);
    }


    public function generate_dealer_billing($start_date = NULL, $end_date = NULL)
    {
       print_r($start_date);
       print_r($end_date);
       exit;
        if($start_date == NULL)
        {
            $start_date = date('Y-m-d');
        }
        else
        {
            $start_date = str_replace("_","-",$start_date);            
        }
        if($end_date == NULL)
        {
            $end_date = date('Y-m-d');
        }
        else
        {
            $end_date = str_replace("_","-",$end_date);            
        }
        $sql = <<<EOF
        SELECT
        generate_crosstab_sql_plain (
        $$ SELECT v.id, v.vehicle_name, v.variant_name, v.color_name, b.dealer_name, count(b.dealer_name) FROM view_dms_vehicles v LEFT JOIN view_report_monthwise_dispatch b on ((v.vehicle_id = b.vehicle_id AND v.variant_id = b.variant_id AND v.color_id = b.color_id) and b.dispatched_date >= '{$start_date}' and b.dispatched_date <= '{$end_date}') where v.deleted_at IS NULL GROUP BY 1,2,3,4,5,v.rank ORDER BY v.rank $$,
        $$ SELECT name as dealer_name from dms_dealers ORDER BY 1 $$,
        'INT',
        '"RANK" TEXT, "Model" TEXT, "Variant" TEXT,"Color" TEXT') AS sqlstring 

EOF;
        $res = $this->db->query($sql)->row_array();
        $result_sql = $this->db->query($res['sqlstring'])->result_array();

        $this->db->order_by('rank');
        $month = $this->nepali_month_model->findAll(NULL,array('name','rank'));

        $this->load->model('dealers/dealer_model');
        $this->db->order_by('name');
        $cities = $this->dealer_model->findAll(null,'name');


        $this->load->library('Excel');


        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(32);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1','Dealer Billed');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Model');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2','Variant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2','Color');

        $row = 2;
        $col = 3;
        foreach ($cities as $key => $value) 
        {
            $currentColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
            $objPHPExcel->getActiveSheet()->getColumnDimension($currentColn)->setWidth(4);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value->name);
            $col++;
        }

        $row = 3;
        $col = 0;
        foreach($result_sql as $key => $values) 
        {           
            $i = 0;
            foreach ($values as $k => $value) {
                if($i != 0){
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $i = 1;

            }
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, '=SUM(D'.$row.':BL'.($row).')');
            $col = 0;
            $row++;      

        }
        $highestColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $objPHPExcel->getActiveSheet()->getStyle('D2:'.$highestColn.'2')->getAlignment()->setTextRotation(90);

        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Dealer Billing.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }


   public function generate_dealer_retail($start_date = NULL, $end_date = NULL)
    {
        if($start_date == NULL)
        {
            $start_date = date('Y-m-d');
        }
        else
        {
            $start_date = str_replace("_","-",$start_date);            
        }
        if($end_date == NULL)
        {
            $end_date = date('Y-m-d');
        }
        else
        {
            $end_date = str_replace("_","-",$end_date);            
        }
         $sql = <<<EOF
           SELECT generate_crosstab_sql_plain (
                $$ SELECT v.id, v.vehicle_name, v.variant_name, v.color_name, b.dealer_name, count(b.dealer_name) FROM view_dms_vehicles v LEFT JOIN view_customers b on ((v.vehicle_id = b.vehicle_id AND v.variant_id = b.variant_id AND v.color_id = b.color_id and b.status_id = 15 )  and b.vehicle_delivery_date >= '{$start_date}' and b.vehicle_delivery_date <= '{$end_date}') where v.deleted_at IS NULL  GROUP BY 1,2,3,4,5,v.rank ORDER BY v.rank $$,
                $$ SELECT name as dealer_name from dms_dealers ORDER BY 1 $$,
                'INT',
                '"RANK" TEXT, "Model" TEXT, "Variant" TEXT,"Color" TEXT') AS sqlstring 

EOF;
            $res = $this->db->query($sql)->row_array();
            $result_sql = $this->db->query($res['sqlstring'])->result_array();

            echo $this->db->last_query();
        $this->db->order_by('rank');
        $month = $this->nepali_month_model->findAll(NULL,array('name','rank'));

        $this->load->model('dealers/dealer_model');
        $this->db->order_by('name');
        $cities = $this->dealer_model->findAll(null,'name');


        $this->load->library('Excel');


        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(32);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1','Dealer Retail');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Model');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2','Variant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2','Color');
  
        $row = 2;
        $col = 3;
        foreach ($cities as $key => $value) 
        {
            $currentColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
            $objPHPExcel->getActiveSheet()->getColumnDimension($currentColn)->setWidth(4);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value->name);
            $col++;
        }

        $row = 3;
        $col = 0;
        foreach($result_sql as $key => $values) 
        {           
            $i = 0;
            foreach ($values as $k => $value) {
                if($i != 0){
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $i = 1;

            }
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, '=SUM(D'.$row.':BL'.($row).')');
            $col = 0;
            $row++;      
           
        }
        $highestColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $objPHPExcel->getActiveSheet()->getStyle('D2:'.$highestColn.'2')->getAlignment()->setTextRotation(90);

        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Dealer Retail.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }

     public function generate_dealer_stock($start_date = NULL, $end_date = NULL)
    {
        if($start_date == NULL)
        {
            $start_date = date('Y-m-d');
        }
        else
        {
            $start_date = str_replace("_","-",$start_date);            
        }
        if($end_date == NULL)
        {
            $end_date = date('Y-m-d');
        }
        else
        {
            $end_date = str_replace("_","-",$end_date);            
        }
         $sql = <<<EOF
            SELECT
        generate_crosstab_sql_plain (
        $$ SELECT v.id, v.vehicle_name, v.variant_name, v.color_name, b.dealer_name, count(b.dealer_name) FROM view_dms_vehicles v LEFT JOIN view_report_billing_stock_ec_list b on (v.vehicle_id = b.vehicle_id AND v.variant_id = b.variant_id AND v.color_id = b.color_id and current_status = 'Bill' and b.dealer_received_date >= '{$start_date}' and b.dealer_received_date <= '{$end_date}') where v.deleted_at IS NULL  GROUP BY 1,2,3,4,5,v.rank ORDER BY v.rank $$,
        $$ SELECT name as dealer_name from dms_dealers ORDER BY 1 $$,
        'INT',
        '"RANK" TEXT, "Model" TEXT, "Variant" TEXT,"Color" TEXT') AS sqlstring 

EOF;
            $res = $this->db->query($sql)->row_array();
            $result_sql = $this->db->query($res['sqlstring'])->result_array();

        $this->db->order_by('rank');
        $month = $this->nepali_month_model->findAll(NULL,array('name','rank'));

        $this->load->model('dealers/dealer_model');
        $this->db->order_by('name');
        $cities = $this->dealer_model->findAll(null,'name');


        $this->load->library('Excel');


        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(32);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1','Dealer Stock');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Model');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2','Variant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2','Color');
  
        $row = 2;
        $col = 3;
        foreach ($cities as $key => $value) 
        {
            $currentColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
            $objPHPExcel->getActiveSheet()->getColumnDimension($currentColn)->setWidth(4);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value->name);
            $col++;
        }

        $row = 3;
        $col = 0;
        foreach($result_sql as $key => $values) 
        {           
            $i = 0;
            foreach ($values as $k => $value) {
                if($i != 0){
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $i = 1;

            }
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, '=SUM(D'.$row.':BL'.($row).')');
            $col = 0;
            $row++;      
           
        }
        $highestColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $objPHPExcel->getActiveSheet()->getStyle('D2:'.$highestColn.'2')->getAlignment()->setTextRotation(90);

        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Dealer Stock.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }

    public function generate_unplanned_order($start_date = NULL,$end_date = NULL)
    {
        $this->msil_order_model->_table = "view_report_msil_order";
        $rows = $this->msil_order_model->findAll(array('order_type'=>'Unplanned'));

        $this->load->library('Excel');

        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);

        $style = array(
        'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
        );

        $objPHPExcel->getDefaultStyle()->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->getStyle("A1:N2")->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
        $objPHPExcel->getActiveSheet()->SetCellValue('A1','UNPLANNED REPORT');

        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Vehicle Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2','Variant Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2','Color Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('D2','Unplanned Quantity');
        $objPHPExcel->getActiveSheet()->SetCellValue('E2','Year');
        $objPHPExcel->getActiveSheet()->SetCellValue('F2','Month');

        $row = 3;
        $col = 0;

        foreach($rows as $key => $values) 
        {   
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->vehicle_name);
            $col++;        
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->variant_name);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->color_name);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->total_order_quantity);
            $col++;      
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->year);
            $col++;      
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $values->month);
            $col++;      
            $col = 0;
            $row++;        
        }

        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=Unplanned Orde.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }

        public function generate_cg_stock($start_date = NULL, $end_date = NULL)
    {
        if($start_date == NULL)
        {
            $start_date = date('Y-m-d');
        }
        else
        {
            $start_date = str_replace("_","-",$start_date);            
        }
        if($end_date == NULL)
        {
            $end_date = date('Y-m-d');
        }
        else
        {
            $end_date = str_replace("_","-",$end_date);            
        }
              $sql = <<<EOF
           SELECT generate_crosstab_sql_plain (
                $$ SELECT v.id, v.vehicle_name, v.variant_name, v.color_name, b.current_location, count(b.current_location) FROM view_dms_vehicles v LEFT JOIN view_log_stock_record_working b on (v.vehicle_id = b.vehicle_id AND v.variant_id = b.variant_id AND v.color_id = b.color_id and (current_status ='Stock' OR current_status = 'repaired stock' OR current_status='Custom' OR current_status = 'Transit')) GROUP BY 1,2,3,4,5,v.rank ORDER BY v.rank $$,
                $$ SELECT current_location from view_log_stock_record_working where (current_status ='Stock' OR current_status = 'repaired stock' OR current_status='Custom' OR current_status = 'Transit') AND current_location IS NOT NULL group by 1 ORDER BY 1 $$,
                'INT',
                '"RANK" TEXT, "Model" TEXT, "Variant" TEXT,"Color" TEXT') AS sqlstring 
EOF;
            $res = $this->db->query($sql)->row_array();
            $result_sql = $this->db->query($res['sqlstring'])->result_array();
      // exit;

        $this->db->order_by('rank');
        $month = $this->nepali_month_model->findAll(NULL,array('name','rank'));

        $this->load->model('dealers/dealer_model');
        // $this->db->order_by('name');
        // $cities = $this->dealer_model->findAll(null,'name');

        $this->dealer_model->_table  = "view_log_stock_record_working";
        $this->db->where("current_status ='Stock' OR current_status = 'repaired stock' OR current_status='Custom' OR current_status = 'Transit'");
        $this->db->group_by('current_location');
        $this->db->order_by('current_location');
        $stockyards = $this->dealer_model->findAll(null,'current_location');

        $this->load->library('Excel');


        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(32);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1','Dealer Retail');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Model');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2','Variant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2','Color');
  
        $row = 2;
        $col = 3; 
        foreach ($stockyards as $key => $value) 
        {
            $currentColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
            $objPHPExcel->getActiveSheet()->getColumnDimension($currentColn)->setWidth(4);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value->current_location);
            $col++;
        }

        $row = 3;
        $col = 0;
        foreach($result_sql as $key => $values) 
        {           
            $i = 0;
            foreach ($values as $k => $value) {
                if($i != 0){
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $i = 1;

            }
            //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, '=SUM(D'.$row.':L'.($row).')');
            $col = 0;
            $row++;      
           
        }
        $highestColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $objPHPExcel->getActiveSheet()->getStyle('D2:'.$highestColn.'2')->getAlignment()->setTextRotation(90);

        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=CG Stock.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }

    public function generate_cg_vintage_stock($start_date = NULL, $end_date = NULL)
    {
        if($start_date == NULL)
        {
            $start_date = date('Y-m-d');
        }
        else
        {
            $start_date = str_replace("_","-",$start_date);            
        }
        if($end_date == NULL)
        {
            $end_date = date('Y-m-d');
        }
        else
        {
            $end_date = str_replace("_","-",$end_date);            
        }
         $sql = <<<EOF
           SELECT generate_crosstab_sql_plain (
                $$ SELECT v.id, v.vehicle_name, v.variant_name, v.color_name, b.year, count(b.year) FROM view_dms_vehicles v LEFT JOIN view_log_stock_record_working b on (v.vehicle_id = b.vehicle_id AND v.variant_id = b.variant_id AND v.color_id = b.color_id and (current_status ='Stock' OR current_status = 'repaired stock' OR current_status='Custom' OR current_status = 'transit')) GROUP BY 1,2,3,4,5,v.rank ORDER BY v.rank $$,
                $$ SELECT year from view_log_stock_record_working where (current_status ='Stock' OR current_status = 'repaired stock' OR current_status='Custom' OR current_location = 'transit') group by 1 ORDER BY 1 $$,
                'INT',
                '"RANK" TEXT, "Model" TEXT, "Variant" TEXT,"Color" TEXT') AS sqlstring 
EOF;
            $res = $this->db->query($sql)->row_array();
            $result_sql = $this->db->query($res['sqlstring'])->result_array();


        $this->load->model('dealers/dealer_model');
        $this->dealer_model->_table = 'view_log_stock_record_working';
        $this->db->order_by('year');
        $this->db->group_by('year');
        $cities = $this->dealer_model->findAll(null,'year');


        $this->load->library('Excel');


        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(32);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1','CG Vintage Stock');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Model');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2','Variant');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2','Color');
  
        $row = 2;
        $col = 3;
        foreach ($cities as $key => $value) 
        {
            $currentColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
            $objPHPExcel->getActiveSheet()->getColumnDimension($currentColn)->setWidth(4);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value->year);
            $col++;
        }

        $row = 3;
        $col = 0;
        foreach($result_sql as $key => $values) 
        {           
            $i = 0;
            foreach ($values as $k => $value) {
                if($i != 0){
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                    $col++;
                }
                $i = 1;

            }
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, '=SUM(D'.$row.':AO'.($row).')');
            $col = 0;
            $row++;      
           
        }
        $highestColn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $objPHPExcel->getActiveSheet()->getStyle('D2:'.$highestColn.'2')->getAlignment()->setTextRotation(90);

        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment;filename=CG Vintage Stock.xls");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
    }
}
