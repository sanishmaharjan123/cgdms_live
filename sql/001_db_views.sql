CREATE EXTENSION IF NOT EXISTS tablefunc;

DROP VIEW IF EXISTS view_users CASCADE;

-- view_group_permissions
DROP VIEW IF EXISTS view_group_permissions CASCADE; 

CREATE VIEW view_group_permissions AS (
	SELECT
		gp.group_id AS group_id,
		gp.perm_id AS perm_id,
		perm.name AS permission,
		grp.name AS group
	FROM
		aauth_group_permissions gp
	INNER JOIN aauth_groups grp ON (grp.id = gp.group_id)
	INNER JOIN aauth_permissions perm ON (perm.id = gp.perm_id)			
);

-- view_user_permissions
DROP VIEW IF EXISTS view_user_permissions CASCADE; 

CREATE VIEW view_user_permissions AS (
	SELECT
		up.user_id AS user_id,
		up.perm_id AS perm_id,
		perm.name AS permission,
		u.username AS username,
		u.email AS email,
		u.fullname AS fullname
	FROM
		aauth_user_permissions up
	INNER JOIN aauth_users u ON (u.id = up.user_id)
	INNER JOIN aauth_permissions perm ON (perm.id = up.perm_id)
);

-- view_user_groups
DROP VIEW IF EXISTS view_user_groups CASCADE; 

CREATE VIEW view_user_groups AS (
	SELECT 
		ug.user_id AS user_id,
	    ug.group_id AS group_id,
	    g.name AS group,
	    u.username AS username,
	    u.email AS email,
	    u.fullname AS fullname
   	FROM 
   		aauth_user_groups ug
    INNER JOIN aauth_users u ON (u.id = ug.user_id)
    INNER JOIN aauth_groups g ON (g.id = ug.group_id)
);

-- view_users
DROP VIEW IF EXISTS view_users CASCADE; 

CREATE VIEW view_users AS (
	SELECT
		u.id AS id, 
		u.email AS email, 
		u.pass AS pass, 
		u.username AS username, 
		u.fullname AS fullname,
		u.banned AS banned, 
		u.last_login AS last_login, 
		u.last_activity AS last_activity, 
		u.date_created AS date_created, 
		u.forgot_exp AS forgot_exp, 
		u.remember_time AS remember_time, 
		u.remember_exp AS remember_exp, 
		u.verification_code AS verification_code, 
		u.totp_secret AS totp_secret, 
		u.ip_address AS ip_address
	FROM
		aauth_users u
);

-- view_district_mvs 
DROP VIEW IF EXISTS view_district_mvs CASCADE; 

CREATE VIEW view_district_mvs AS (
	SELECT
		t1.id AS id,
		t1.created_by AS created_by,
		t1.updated_by AS updated_by,
		t1.deleted_by AS deleted_by,
		t1.created_at AS created_at,
		t1.updated_at AS updated_at,
		t1.deleted_at AS deleted_at,
		t1.code AS code,
		t1.name AS name,
		t1.parent_id AS parent_id,
		t2.code AS parent_code,
		t2.name AS parent_name,
		t1.type AS type
	FROM
		mst_district_mvs AS t1
	INNER JOIN mst_district_mvs AS t2 ON (t1.parent_id = t2.id)
);

-- view_city_places 
DROP VIEW IF EXISTS view_city_places CASCADE; 

CREATE VIEW view_city_places AS (
	SELECT
		c.id AS id,
		c.created_by AS created_by,
		c.updated_by AS updated_by,
		c.deleted_by AS deleted_by,
		c.created_at AS created_at,
		c.updated_at AS updated_at,
		c.deleted_at AS deleted_at,
		c.name AS name,
		c.district_id AS district_id,
		c.mun_vdc_id AS mun_vdc_id,
		d.name AS district_name,
		mv.name AS mun_vdc_name
	FROM
		mst_city_places AS c
	 INNER JOIN mst_district_mvs AS d ON (c.district_id = d.id)
	 INNER JOIN mst_district_mvs AS mv ON (c.mun_vdc_id = mv.id)
);


-- view_mst_vehicles 
DROP VIEW IF EXISTS view_mst_vehicles CASCADE; 

CREATE VIEW view_mst_vehicles AS (
	SELECT
		v.id AS id,
		v.created_by AS created_by,
		v.updated_by AS updated_by,
		v.deleted_by AS deleted_by,
		v.created_at AS created_at,
		v.updated_at AS updated_at,
		v.deleted_at AS deleted_at,
		v.firm_id AS firm_id,
		v.name AS name,
		v.rank AS rank,
		f.name AS firm_name
	FROM
		mst_vehicles AS v
	 INNER JOIN mst_firms AS f ON (v.firm_id = f.id)
);

-- view_dealers 
DROP VIEW IF EXISTS view_dealers CASCADE; 

CREATE VIEW view_dealers AS (
	SELECT
		d.id AS id, 
		d.created_by AS created_by, 
		d.updated_by AS updated_by, 
		d.deleted_by AS deleted_by, 
		d.created_at AS created_at, 
		d.updated_at AS updated_at, 
		d.deleted_at AS deleted_at, 
		d.name AS name, 
		d.incharge_id AS incharge_id,
		d.district_id AS district_id, 
		d.mun_vdc_id AS mun_vdc_id, 
		d.city_place_id AS city_place_id, 
		d.address_1 AS address_1, 
		d.address_2 AS address_2, 
		d.phone_1 AS phone_1, 
		d.phone_2 AS phone_2, 
		d.email AS email, 
		d.fax AS fax, 
		d.latitude AS latitude, 
		d.longitude AS longitude, 
		d.remarks AS remarks,
		c.district_name AS district_name,
		c.mun_vdc_name AS mun_vdc_name,
		c.name AS city_name,
		u.username AS incharge_name
	FROM
		dms_dealers AS d
	INNER JOIN view_city_places AS c ON (c.id = d.city_place_id)
	LEFT JOIN aauth_users AS u ON (d.incharge_id = u.id)
);

-- view_employees 
DROP VIEW IF EXISTS view_employees CASCADE; 

CREATE VIEW view_employees AS (
	SELECT
		e.id AS id,
		e.created_by AS created_by,
		e.updated_by AS updated_by,
		e.deleted_by AS deleted_by,
		e.created_at AS created_at,
		e.updated_at AS updated_at,
		e.deleted_at AS deleted_at,
		e.dealer_id AS dealer_id,
		e.has_login AS has_login,
		e.user_id AS user_id,
		e.first_name AS first_name,
		e.middle_name AS middle_name,
		e.last_name AS last_name,
		e.dob_en AS dob_en,
		e.dob_np AS dob_np,
		e.gender AS gender,
		e.marital_status AS marital_status,
		e.permanent_district_id AS permanent_district_id,
		e.permanent_mun_vdc_id AS permanent_mun_vdc_id,
		e.permanent_ward AS permanent_ward,
		e.permanent_address_1 AS permanent_address_1,
		e.permanent_address_2 AS permanent_address_2,
		e.temporary_district_id AS temporary_district_id,
		e.temporary_mun_vdc_id AS temporary_mun_vdc_id,
		e.temporary_ward AS temporary_ward,
		e.temporary_address_1 AS temporary_address_1,
		e.temporary_address_2 AS temporary_address_2,
		e.home AS home,
		e.work AS work,
		e.mobile AS mobile,
		e.work_email AS work_email,
		e.personal_email AS personal_email,
		e.photo AS photo,
		e.nationality AS nationality,
		e.citizenship_no AS citizenship_no,
		e.citizenship_issued_on AS citizenship_issued_on,
		e.citizenship_issued_by AS citizenship_issued_by,
		e.license AS license,
		e.license_type AS license_type,
		e.license_no AS license_no,
		e.license_issued_on AS license_issued_on,
		e.license_issued_by AS license_issued_by,
		e.license_expiry AS license_expiry,
		e.passport AS passport,
		e.passport_type AS passport_type,
		e.passport_no AS passport_no,
		e.passport_issued_on AS passport_issued_on,
		e.passport_issued_by AS passport_issued_by,
		e.passport_expiry AS passport_expiry,
		e.education_id AS education_id,
		e.designation_id AS designation_id,
		e.interview_date_en AS interview_date_en,
		e.interview_date_np AS interview_date_np,
		e.probation_period AS probation_period,
		e.joining_date_en AS joining_date_en,
		e.joining_date_np AS joining_date_np,
		e.confirmation_date_en AS confirmation_date_en,
		e.confirmation_date_np AS confirmation_date_np,
		e.leaving_date_en AS leaving_date_en,
		e.leaving_date_np AS leaving_date_np,
		e.leaving_reason AS leaving_reason,
		CASE WHEN e.middle_name <> '' THEN e.first_name || ' ' || e.middle_name || ' ' || e.last_name ELSE e.first_name || ' ' || e.last_name END AS employee_name, 
		v.username,
		v.group_id AS group_id,
		v.group AS group_name,
		d1.name AS permanent_district_name,
		mv1.name AS permanent_mun_vdc_name,
		d2.name AS temporary_district_name,
		mv2.name AS temporary_mun_vdc_name,
		d.name AS designation_name,
		ed.name AS education_name,
		dl.name AS dealer_name
	FROM
		dms_employees AS e
	LEFT JOIN view_user_groups AS v ON (e.user_id = v.user_id AND v.group_id <> 1)
	INNER JOIN mst_district_mvs AS d1 ON (e.permanent_district_id = d1.id)
	INNER JOIN mst_district_mvs AS mv1 ON (e.permanent_mun_vdc_id= mv1.id)
	LEFT JOIN mst_district_mvs AS d2 ON (e.temporary_district_id = d2.id)
	LEFT JOIN mst_district_mvs AS mv2 ON (e.temporary_mun_vdc_id = mv2.id)
	LEFT JOIN mst_designations AS d ON (e.designation_id = d.id)
	LEFT JOIN mst_educations AS ed ON (e.education_id= ed.id)
	LEFT JOIN dms_dealers AS dl ON (e.dealer_id= dl.id)
);

-- view_employee_contacts 
DROP VIEW IF EXISTS view_employee_contacts CASCADE; 

CREATE VIEW view_employee_contacts AS (
	SELECT
		ec.id AS id,
		ec.created_by AS created_by,
		ec.updated_by AS updated_by,
		ec.deleted_by AS deleted_by,
		ec.created_at AS created_at,
		ec.updated_at AS updated_at,
		ec.deleted_at AS deleted_at,
		ec.employee_id AS employee_id,
		ec.name AS name,
		ec.relation_id AS relation_id,
		ec.home AS home,
		ec.work AS work,
		ec.mobile AS mobile,
		r.name AS relation_name
	FROM
		dms_employee_contacts AS ec
	INNER JOIN mst_relations AS r ON (ec.relation_id = r.id)
);

-- view_customer_status 
DROP VIEW IF EXISTS view_customer_status_latest CASCADE; 

CREATE VIEW view_customer_status_latest AS (
	SELECT 
			dms_cs0.customer_id, dms_cs0.status_id, dms_cs0.created_at::date AS status_date, mis.name,mis.rank, CASE WHEN (dms_cs0.reason_id < 1 OR dms_cs0.reason_id IS NULL) THEN 'N/A' ELSE r.name END AS reason_name, dms_cs0.notes
		FROM 
			dms_customer_statuses dms_cs0 
		INNER JOIN ( 
			SELECT 
				dms_cs1.customer_id, 
				MAX (dms_cs1.created_at) AS latest_date 
			FROM 
				dms_customer_statuses dms_cs1 
			GROUP BY 
				dms_cs1.customer_id
			) AS tbl 
		ON ( tbl.customer_id = dms_cs0.customer_id AND tbl.latest_date = dms_cs0.created_at )
		LEFT JOIN mst_inquiry_statuses mis on (dms_cs0.status_id = mis.id)
		LEFT JOIN mst_reasons r on (dms_cs0.reason_id = r.id )
);

-- view_customers 
DROP VIEW IF EXISTS view_customers CASCADE; 

CREATE VIEW view_customers AS (
	SELECT
		c.id AS id,
		c.created_by AS created_by,
		c.updated_by AS updated_by,
		c.deleted_by AS deleted_by,
		c.created_at AS created_at,
		c.updated_at AS updated_at,
		c.deleted_at AS deleted_at,
		c.inquiry_no AS inquiry_no,
		c.fiscal_year_id AS fiscal_year_id,
		c.inquiry_date_en AS inquiry_date_en,
		c.inquiry_date_np AS inquiry_date_np,
		c.inquiry_kind AS inquiry_kind,
		c.customer_type_id AS customer_type_id,
		c.first_name AS first_name,
		c.middle_name AS middle_name,
		c.last_name AS last_name,
		c.gender AS gender,
		c.marital_status AS marital_status,
		c.family_size AS family_size,
		(DATE_PART('year', now()::date) - DATE_PART('year', c.dob_en::date)) as age,
		c.dob_en AS dob_en,
		c.dob_np AS dob_np,
		c.anniversary_en AS anniversary_en,
		c.anniversary_np AS anniversary_np,
		c.district_id AS district_id,
		c.mun_vdc_id AS mun_vdc_id,
		c.address_1 AS address_1,
		c.address_2 AS address_2,
		c.email AS email,
		c.home_1 AS home_1,
		c.home_2 AS home_2,
		c.work_1 AS work_1,
		c.work_2 AS work_2,
		c.mobile_1 AS mobile_1,
		c.mobile_2 AS mobile_2,
		c.pref_communication AS pref_communication,
		c.occupation_id AS occupation_id,
		c.education_id AS education_id,
		c.dealer_id AS dealer_id,
		c.executive_id AS executive_id,
		c.payment_mode_id AS payment_mode_id,
		c.source_id AS source_id,
		m11.status_id AS status_id,
		c.contact_1_name AS contact_1_name,
		c.contact_1_mobile AS contact_1_mobile,
		c.contact_1_relation_id AS contact_1_relation_id,
		c.contact_2_name AS contact_2_name,
		c.contact_2_mobile AS contact_2_mobile,
		c.contact_2_relation_id AS contact_2_relation_id,
		c.remarks AS remarks,
		c.vehicle_id AS vehicle_id,
		c.variant_id AS variant_id,
		c.color_id AS color_id,
		c.walkin_source_id AS walkin_source_id,
		c.event_id AS event_id,
		c.institution_id AS institution_id,
        c.exchange_car_make AS exchange_car_make,
		c.exchange_car_model AS exchange_car_model,
		c.exchange_car_year AS exchange_car_year,
		c.exchange_car_kms AS exchange_car_kms,
		c.exchange_car_value AS exchange_car_value,
		c.exchange_car_bonus AS exchange_car_bonus,
		c.exchange_total_offer AS exchange_total_offer,
		c.bank_id AS bank_id,
		c.bank_branch AS bank_branch,
		c.bank_staff AS bank_staff,
		c.bank_contact AS bank_contact,
		CASE WHEN c.middle_name <> '' THEN c.first_name || ' ' || c.middle_name || ' ' || c.last_name ELSE c.first_name || ' ' || c.last_name END AS full_name,
		(substr(m1.nepali_start_date, 0, 5) || '-' || substr(m1.nepali_end_date, 3, 2)) AS fiscal_year,
		m2.name AS customer_type_name,
		m2.rank AS customer_type_rank,
		replace(m3.parent_name, ' Zone', '') AS zone_name,
		m3.name AS district_name,
		m4.name AS mun_vdc_name,
		m5.name AS occupation_name,
		m6.name AS education_name,
		m7.name AS dealer_name,
		CASE WHEN m8.middle_name <> '' THEN m8.first_name || ' ' || m8.middle_name || ' ' || m8.last_name ELSE m8.first_name || ' ' || m8.last_name END AS executive_name,
		m9.name AS payment_mode_name,
		m10.name AS source_name,
		m10.rank AS source_rank,
		m11.name AS actual_status_name,
		m11.rank AS actual_status_rank,
		(CASE 
			WHEN m11.status_id = 4 THEN 'Retail Finance' 
			WHEN m11.status_id = 5 THEN 'Retail Finance' 
			WHEN m11.status_id = 6 THEN 'Retail Finance' 
			WHEN m11.status_id = 7 THEN 'Retail Finance' 
			WHEN m11.status_id = 8 THEN 'Retail Finance' 
			WHEN m11.status_id = 9 THEN 'Retail Finance' 
			WHEN m11.status_id = 10 THEN 'Retail Finance' 
			WHEN m11.status_id = 11 THEN 'Retail Finance' 
			WHEN m11.status_id = 12 THEN 'Retail Finance' 
			WHEN m11.status_id = 13 THEN 'Retail Finance' 
			WHEN m11.status_id = 14 THEN 'Retail Finance' 
			ELSE m11.name
		END)::text AS status_name,
		(CASE 
			WHEN m11.status_id = 4 THEN 99
			WHEN m11.status_id = 5 THEN 99
			WHEN m11.status_id = 6 THEN 99
			WHEN m11.status_id = 7 THEN 99
			WHEN m11.status_id = 8 THEN 99
			WHEN m11.status_id = 9 THEN 99
			WHEN m11.status_id = 10 THEN 99
			WHEN m11.status_id = 11 THEN 99
			WHEN m11.status_id = 12 THEN 99
			WHEN m11.status_id = 13 THEN 99
			WHEN m11.status_id = 14 THEN 99
			ELSE m11.rank
		END)::text AS status_rank,
		m11.status_date AS status_date,
		m11.reason_name AS reason_name,
		m12.name AS contact_1_relation_name,
		m13.name AS contact_2_relation_name,
		m14.name AS vehicle_name,
		m14.rank AS vehicle_rank,
		m15.name AS variant_name,
		(m16.name || ' (' || m16.code || ')') AS color_name,
		(CASE WHEN c.walkin_source_id IS NOT NULL THEN m17.name ELSE 'N/A' END ) AS walkin_source_name,
		(CASE WHEN c.event_id IS NOT NULL THEN m18.name ELSE 'N/A' END ) AS event_name,
		(CASE WHEN c.institution_id IS NOT NULL THEN m19.name ELSE 'N/A' END ) AS institution_name,
		(CASE WHEN c.bank_id IS NOT NULL THEN m20.name ELSE 'Others' END ) AS bank_name,
		(CASE WHEN m21.id IS NULL THEN 'NOT TAKEN' ELSE 'TAKEN' END)::text AS test_drive,
		(CASE 
			WHEN c.source_id = 1 THEN m17.name 
			WHEN c.source_id = 2 THEN m18.name ELSE 'Referral'
		END
		)::text AS inquiry_type

	FROM
		dms_customers AS c
	LEFT JOIN mst_fiscal_years AS m1 ON (c.fiscal_year_id = m1.id)
	LEFT JOIN mst_customer_types AS m2 ON (c.customer_type_id = m2.id)
	LEFT JOIN view_district_mvs AS m3 ON (c.district_id = m3.id)
	LEFT JOIN mst_district_mvs AS m4 ON (c.mun_vdc_id = m4.id)
	LEFT JOIN mst_occupations AS m5 ON (c.occupation_id = m5.id)
	LEFT JOIN mst_educations AS m6 ON (c.education_id = m6.id)
	LEFT JOIN dms_dealers AS m7 ON (c.dealer_id = m7.id)
	LEFT JOIN dms_employees AS m8 ON (c.executive_id = m8.id)
	LEFT JOIN mst_payment_modes AS m9 ON (c.payment_mode_id = m9.id)
	LEFT JOIN mst_sources AS m10 ON (c.source_id = m10.id)
	LEFT JOIN view_customer_status_latest AS m11 ON (c.id = m11.customer_id)
	LEFT JOIN mst_relations AS m12 ON (c.contact_1_relation_id = m12.id)
	LEFT JOIN mst_relations AS m13 ON (c.contact_2_relation_id = m13.id)
	LEFT JOIN mst_vehicles AS  m14 ON (c.vehicle_id = m14.id)
	LEFT JOIN mst_variants AS m15 ON (c.variant_id = m15.id)
	LEFT JOIN mst_colors AS m16 ON (c.color_id = m16.id)
	LEFT JOIN mst_walkin_sources AS m17 ON (c.walkin_source_id = m17.id)
	LEFT JOIN dms_events AS m18 ON (c.event_id = m18.id)
	LEFT JOIN mst_institutions AS m19 ON (c.institution_id = m19.id)
	LEFT JOIN mst_banks AS m20 ON (c.bank_id = m20.id)
	LEFT JOIN dms_customer_test_drives AS m21 ON (c.id = m21.customer_id)
	-- LEFT JOIN dms_quotations AS m22 ON (c.id = m22.customer_id)
);

-- view_customer_statuses 
DROP VIEW IF EXISTS view_customer_statuses CASCADE; 

CREATE VIEW view_customer_statuses AS (
	SELECT
		ls.id AS id,
		ls.created_by AS created_by,
		ls.updated_by AS updated_by,
		ls.deleted_by AS deleted_by,
		ls.created_at AS created_at,
		ls.updated_at AS updated_at,
		ls.deleted_at AS deleted_at,
		ls.customer_id AS customer_id,
		ls.status_id AS status_id,
		ls.reason_id AS reason_id,
		ls.duration AS duration,
		ls.notes AS notes,
		s.name AS status_name,
		r.name AS reason_name
	FROM
		dms_customer_statuses AS ls
	LEFT JOIN mst_inquiry_statuses AS s ON (ls.status_id = s.id)
	LEFT JOIN mst_reasons AS r ON (ls.reason_id = r.id)
		
);

-- view_customer_followups 
DROP VIEW IF EXISTS view_customer_followups CASCADE; 

CREATE VIEW view_customer_followups AS (
	SELECT
		lf.id AS id,
		lf.created_by AS created_by,
		lf.updated_by AS updated_by,
		lf.deleted_by AS deleted_by,
		lf.created_at AS created_at,
		lf.updated_at AS updated_at,
		lf.deleted_at AS deleted_at,
		lf.customer_id AS customer_id,
		lf.executive_id AS executive_id,
		lf.followup_date_en AS followup_date_en,
		lf.followup_date_np AS followup_date_np,
		lf.followup_mode AS followup_mode,
		lf.followup_status AS followup_status,
		lf.followup_notes AS followup_notes,
		lf.next_followup AS next_followup,
		lf.next_followup_date_en AS next_followup_date_en,
		lf.next_followup_date_np AS next_followup_date_np,
		CASE WHEN e.middle_name <> '' THEN e.first_name || ' ' || e.middle_name || ' ' || e.last_name ELSE e.first_name || ' ' || e.last_name END AS executive_name
	FROM
		dms_customer_followups AS lf
	INNER JOIN dms_employees AS e ON (lf.executive_id = e.id)
		
);

-- view_dms_vehicles 
DROP VIEW IF EXISTS view_dms_vehicles CASCADE; 

CREATE VIEW view_dms_vehicles AS (
	SELECT
		dv.id AS id,
		dv.created_by AS created_by,
		dv.updated_by AS updated_by,
		dv.deleted_by AS deleted_by,
		dv.created_at AS created_at,
		dv.updated_at AS updated_at,
		dv.deleted_at AS deleted_at,
		dv.vehicle_id AS vehicle_id,
		dv.variant_id AS variant_id,
		dv.color_id,
		ve.name AS vehicle_name,
		va.name AS variant_name,
		(co.name || ' (' || co.code || ')') AS color_name
	FROM
		dms_vehicles AS dv
	LEFT JOIN mst_vehicles AS  ve ON (dv.vehicle_id = ve.id)
	LEFT JOIN mst_variants AS va ON (dv.variant_id = va.id)
	LEFT JOIN mst_colors AS co ON (dv.color_id = co.id)
);

-- view_dms_events 
DROP VIEW IF EXISTS view_dms_events CASCADE; 

CREATE VIEW view_dms_events AS (
	SELECT
		e.id AS id,
		e.created_by AS created_by,
		e.updated_by AS updated_by,
		e.deleted_by AS deleted_by,
		e.created_at AS created_at,
		e.updated_at AS updated_at,
		e.deleted_at AS deleted_at,
		e.dealer_id AS dealer_id,
		e.name AS name,
		e.start_date_en AS start_date_en,
		e.start_date_np AS start_date_np,
		e.end_date_en AS end_date_en,
		e.end_date_np AS end_date_np,
		e.active AS active,
		d.name AS dealer_name
	FROM
		dms_events AS e
	LEFT JOIN dms_dealers AS d ON d.id = e.dealer_id
);

-- view_customer_test_drives 
DROP VIEW IF EXISTS view_customer_test_drives CASCADE; 

CREATE VIEW view_customer_test_drives AS (
	SELECT
		td.id AS id,
		td.created_by AS created_by,
		td.updated_by AS updated_by,
		td.deleted_by AS deleted_by,
		td.created_at AS created_at,
		td.updated_at AS updated_at,
		td.deleted_at AS deleted_at,
		td.td_date_en AS td_date_en,
		td.td_date_np AS td_date_np,
		td.td_time AS td_time,
		td.customer_id AS customer_id,
		td.executive_id AS executive_id,
		td.vehicle_id AS vehicle_id,
		td.variant_id AS variant_id,
		td.mileage_start AS mileage_start,
		td.mileage_end AS mileage_end,
		td.duration AS duration,
		td.td_location AS td_location,
		va.name AS variant_name,
		ve.name AS vehicle_name,
		CASE WHEN e.middle_name <> '' THEN e.first_name || ' ' || e.middle_name || ' ' || e.last_name ELSE e.first_name || ' ' || e.last_name END AS executive_name
	FROM
		dms_customer_test_drives AS td
	LEFT JOIN mst_variants AS va ON td.variant_id = va.id  
	LEFT JOIN mst_vehicles AS ve ON td.vehicle_id = ve.id
	LEFT JOIN dms_employees AS e ON td.executive_id = e.id
);

-- view_quotations
DROP VIEW IF EXISTS view_quotations CASCADE; 

CREATE VIEW view_quotations AS (
	SELECT
		q.id AS id,
		q.created_by AS created_by,
		q.updated_by AS updated_by,
		q.deleted_by AS deleted_by,
		q.created_at AS created_at,
		q.updated_at AS updated_at,
		q.deleted_at AS deleted_at,
		q.customer_id AS customer_id,
		q.quotation_date_en AS quotation_date_en,
		q.quotation_date_np AS quotation_date_np,
		q.quote_mrp AS quote_mrp,
		q.quote_discount AS quote_discount,
		q.quote_price AS quote_price,
		q.quote_unit AS quote_unit,
		c.first_name AS first_name,
		c.middle_name AS middle_name,
		c.last_name AS last_name,
		c.bank_name AS bank_name,
		c.address_1 AS address_1,
		c.address_2 AS address_2,
		c.home_1 AS home_1,
		c.mobile_1 AS mobile_1,
		c.work_1 AS work_1,
		c.vehicle_id AS vehicle_id,
		c.vehicle_name AS vehicle_name,
		c.variant_name AS variant_name,
		c.color_name AS color_name,
		v.firm_name as firm_name
	FROM
		dms_quotations AS q
	LEFT JOIN view_customers AS c ON (q.customer_id = c . id)
	LEFT JOIN view_mst_vehicles AS v ON (c.vehicle_id = v.id)

);

-- view_inquiry_retail
DROP VIEW IF EXISTS view_inquiry_retail CASCADE; 

CREATE VIEW view_inquiry_retail AS (
	SELECT
		*,
		( CASE 
			WHEN age IS NULL THEN 'N/A'
			WHEN age < 30 THEN 'Age 00-30'
			WHEN age >= 30 AND age <=40 THEN 'Age 30-40'
			WHEN age >= 40 AND age <=50 THEN 'Age 40-50'
			WHEN age >= 50 AND age <=60 THEN 'Age 50-60'
			ELSE 'Age 60+'
			END
		) AS age_group
	FROM
		view_customers c
	WHERE
		c.status_id = 15
);

-- view_test_drive_conversion_ratio
DROP VIEW IF EXISTS view_test_drive_conversion_ratio CASCADE; 

CREATE VIEW view_test_drive_conversion_ratio AS (
	SELECT
		* 
	FROM
		view_customers c
	WHERE
		c.status_id = 15 AND c.test_drive = 'TAKEN'
);

-- view_inquiry_pending
DROP VIEW IF EXISTS view_inquiry_pending CASCADE; 

CREATE VIEW view_inquiry_pending AS (
	SELECT
		* 
	FROM
		view_customers c
	WHERE
		c.status_id = 1
);

-- view_inquiry_lost
DROP VIEW IF EXISTS view_inquiry_lost CASCADE; 

CREATE VIEW view_inquiry_lost AS (
	SELECT
		* 
	FROM
		view_customers c
	WHERE
		c.status_id = 16
);

-- view_inquiry_non_purchase
DROP VIEW IF EXISTS view_inquiry_non_purchase CASCADE; 

CREATE VIEW view_inquiry_non_purchase AS (
	SELECT
		* 
	FROM
		view_customers c
	WHERE
		c.status_id in (17,18)
);

-- view_customer_status_dates
DROP VIEW IF EXISTS view_customer_status_dates CASCADE; 

CREATE VIEW view_customer_status_dates AS (

	SELECT * from crosstab (
		$$ SELECT customer_id, status_id, MAX(created_at::DATE) AS status_date FROM view_customer_statuses GROUP BY 1,2 ORDER BY 1,2 $$,
		$$ SELECT DISTINCT id FROM mst_inquiry_statuses order by 1 $$
	)
	AS temp_table
	(
		"customer_id" INT,
		"status_1" TEXT,
		"status_2" TEXT,
		"status_3" TEXT,
		"status_4" TEXT,
		"status_5" TEXT,
		"status_6" TEXT,
		"status_7" TEXT,
		"status_8" TEXT,
		"status_9" TEXT,
		"status_10" TEXT,
		"status_11" TEXT,
		"status_12" TEXT,
		"status_13" TEXT,
		"status_14" TEXT,
		"status_15" TEXT,
		"status_16" TEXT,
		"status_17" TEXT,
		"status_18" TEXT
	)
);


-- view_customers_all_status
DROP VIEW IF EXISTS view_customers_all_status CASCADE; 

CREATE VIEW view_customers_all_status AS (
	SELECT * from view_customers ir 
	JOIN view_customer_status_dates csd on (ir.id = csd.customer_id)
);

-- view_retail_finance
DROP VIEW IF EXISTS view_retail_finance CASCADE; 

CREATE VIEW view_retail_finance AS (
	SELECT * from view_inquiry_retail ir 
	JOIN view_customer_status_dates csd on (ir.id = csd.customer_id and ir.payment_mode_id = 2)
);


-- view_followup_schedule 
DROP VIEW IF EXISTS view_followup_schedule CASCADE; 

CREATE VIEW view_followup_schedule AS (
	SELECT
		cf.id AS id,
		cf.created_by AS created_by,
		cf.updated_by AS updated_by,
		cf.deleted_by AS deleted_by,
		cf.created_at AS created_at,
		cf.updated_at AS updated_at,
		cf.deleted_at AS deleted_at,
		cf.customer_id AS customer_id,
		cf.executive_id AS executive_id,
		cf.followup_date_en AS followup_date_en,
		cf.followup_date_np AS followup_date_np,
		cf.followup_mode AS followup_mode,
		cf.followup_status AS followup_status,
		cf.followup_notes AS followup_notes,
		cf.next_followup AS next_followup,
		cf.next_followup_date_en AS next_followup_date_en,
		cf.next_followup_date_np AS next_followup_date_np,
		c.inquiry_no AS inquiry_no,
		c.inquiry_date_en AS inquiry_date_en,
		c.inquiry_date_np AS inquiry_date_np,
		c.dealer_id AS dealer_id,
		c.vehicle_name AS vehicle_name,
		c.variant_name AS variant_name,
		c.color_name AS color_name,
		c.status_name AS status_name,
		c.dealer_name,
		CASE WHEN e.middle_name <> '' THEN e.first_name || ' ' || e.middle_name || ' ' || e.last_name ELSE e.first_name || ' ' || e.last_name END AS executive_name,
		CASE WHEN c.middle_name <> '' THEN c.first_name || ' ' || c.middle_name || ' ' || c.last_name ELSE c.first_name || ' ' || c.last_name END AS customer_name
	FROM
		dms_customer_followups AS cf
	LEFT JOIN view_customers AS c ON c.id = cf.customer_id
	LEFT JOIN dms_employees AS e ON (cf.executive_id = e.id)
	WHERE cf.followup_status = 'Open'
);