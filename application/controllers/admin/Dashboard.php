<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Dashboard
 *
 * Extends the Admin_Controller class
 * 
 */
class Dashboard extends Admin_Controller 
{

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('dashboard');
	}

	public function index()
	{
		$this->lang->load('customers/customer');
		$data['header'] = lang('menu_dashboard');
		$data['stock_yards'] = $this->db->get('mst_stock_yards')->result_array();
		
		$data['page'] = $this->config->item('template_admin') . "index_dashboard";
		
		$this->load->view($this->_container, $data);
	}

	public function sales_dashboard()
	{
		$data['header'] = 'Sales Dashboard';
		$data['page'] = $this->config->item('template_admin') . "dashboard";
		$this->load->view($this->_container, $data);
	}

	public function logistic_dashboard()
	{
		$data['header'] = 'Logistic Dashboard';
		$data['stock_yards'] = $this->db->get('mst_stock_yards')->result_array();
		$data['mfg_year'] = $this->db->query('SELECT distinct year FROM view_msil_dispatch_records ORDER BY year')->result_array();
		$data['page'] = $this->config->item('template_admin') . "dashboard_logistic";
		$this->load->view($this->_container, $data);
	}

	public function dashboard_dealer()
	{
		$data['header'] = 'Logistic Dashboard';
		$data['stock_yards'] = $this->db->get('mst_stock_yards')->result_array();
		$data['page'] = $this->config->item('template_admin') . "dashboard_dealer";
		$this->load->view($this->_container, $data);
	}
}
